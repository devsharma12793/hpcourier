//
//  SLString.swift
//  Beehive
//
//  Created by SoluLab on 01/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation
import UIKit
extension String {

    func validateEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: self)
    }
    

}
