//
//  Branch.swift
//  HPCourier
//
//  Created by Devendra on 05/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import Foundation
class BranchDetail{
    
    var from_branch : Int?
    var from_city_id : Int?
    var from_location : String?
    var from_location_latitude : String?
    var from_location_longitude : String?
    var goods_courier_type : Int?
    var goods_price_per_kg : Int?
    var goods_weight_kg : Int?
    var id : Int?
    var other_detail : String?
    var other_detail_courier_type : String?
    var simple_courier_type : Int?
    var simple_price_per_kg : Int?
    var simple_weight_kg : Int?
    var title : String?
    var to_city_id : Int?
    var to_location : String?
    var to_location_latitude : String?
    var to_location_longitude : String?
/*
    func populateWithJson(dict: Address) {
        
        if let from_branch = dict.from_branch{
            self.from_branch = from_branch
        }
        if let from_city_id = dict.from_city_id{
            self.from_city_id = from_city_id
        }
        if let from_location = dict.from_location{
            self.from_location = from_location
        }
        if let streetNo = dict.streetNo{
            self.streetNo = streetNo
        }
        if let Street = dict.Street{
            self.Street = Street
        }
        if let pincode = dict.pincode{
            self.pincode = pincode
        }
        if let lat = dict.latitute{
            self.latitude = lat
        }
        if let long = dict.longitude{
            self.longitude = long
        }
        if let city = dict.city{
            self.city = city
        }
        if let state = dict.state{
            self.state = state
        }
        if let country = dict.country{
            self.country = country
        }
        
        
    }
    func toJsonDict() -> JSONDictionary {
        
        var dict: JSONDictionary = [:]
        
        if let name = name { dict["name"] = name as AnyObject? }
        
        if let mobile = Mobile { dict["Mobile"] = mobile as AnyObject? }
        
        if let addressString = addressString { dict["addressString"] = addressString as AnyObject? }
        
        if let streetNo = streetNo { dict["streetNo"] = streetNo as AnyObject? }
        
        if let Street = Street { dict["Street"] = Street as AnyObject? }
        
        if let pincode = pincode { dict["pincode"] = pincode as AnyObject? }
        
        if let latitude = latitude { dict["latitude"] = latitude as AnyObject? }
        
        if let longitude = longitude { dict["longitude"] = longitude as AnyObject? }
        
        if let city = city { dict["city"] = city as AnyObject? }
        
        if let state = state { dict["state"] = state as AnyObject? }
        
        if let country = pincode { dict["country"] = country as AnyObject? }
        
        return dict
    }
    
    */
}
