 //
 //  APIManager.swift
 //  Beehive
 //
 //  Created by SoluLab on 07/12/16.
 //  Copyright © 2016 SoluLab. All rights reserved.
 //
 
 import Foundation
 import Alamofire
 
 public class APIManager {
    
    public class var sharedInstance: APIManager {
        struct Singleton {
            static let instance: APIManager = APIManager()
        }
        return Singleton.instance
    }
    
    init() {}
    
    //MARK: Login
    
    func LoginUser(login: Login, completion:@escaping (_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + NewLoginRequestUrl, method: .post, parameters: login.toJsonDict()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json[kstatus] as? String {
                    
                    if status == kOK {
                        
                        if let data = json["result"] as? JSONDictionary {
                            LoggedInUser.sharedInstance.populateWithJson(dict: data)
                            LoggedInUser.sharedInstance.saveUserData(user: data)
                        }
                        completion(nil)
                    } else if status == kINVALID {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(SLError.EmptyResultError)
                    } else if kstatus == kFAILED {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(SLError.EmptyResultError)
                    } else if status == kERROR {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                        completion(SLError.EmptyResultError)
                    }
                }
            } else {
                completion(SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    
    //MARK: Login with social signup
    
    func LoginUserWithSocial(login: Login, completion:@escaping (_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + NewLoginRequestUrl, method: .post, parameters: login.toJsonDictForSocial()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json[kstatus] as? String {
                    
                    if status == kOK {
                        
                        if let data = json["result"] as? JSONDictionary {
                            LoggedInUser.sharedInstance.populateWithJson(dict: data)
                            LoggedInUser.sharedInstance.saveUserData(user: data)
                        }
                        completion(nil)
                    } else if status == kINVALID {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(SLError.EmptyResultError)
                    } else if kstatus == kFAILED {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(SLError.EmptyResultError)
                    } else if status == kERROR {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                        completion(SLError.EmptyResultError)
                    }
                }
            } else {
                completion(SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    
    
    //MARK: Sign Up
    
    func RegisterNewUser(register: Register, completion:@escaping (_ error: NSError?) -> ()) {
        
        
        APPDELEGATE.addChargementLoader()
        print(register.toJsonDict())
        
        Alamofire.request(NewBaseWebRequestUrl + NewSignupRequestUrl, method: .post, parameters: register.toJsonDict()).responseJSON { response in
            APPDELEGATE.removeChargementLoader()
            
            if let json = response.result.value as? JSONDictionary {
                
                //print(json)
                
                let status = json[kstatus] as? String
                if status == kOK {
                    completion(nil)
                    if let mesage = json[kmessage] as? String {
                        SLAlert.showAlert(str: mesage)
                    }
                    
                } else if status == kINVALID {
                    
                    completion(SLError.EmptyResultError)
                } else if kstatus == kFAILED {
                    completion(SLError.EmptyResultError)
                }else if status == kERROR {
                    if let mesage = json[kmessage] as? String {
                        SLAlert.showAlert(str: mesage)
                        
                        
                    }
                }
            }else{
                completion(SLError.EmptyResultError)
            }
            
        }
    }
    
    //MARK: GetBranch
    
    func GetBranch(dictData: JSONDictionary, completion:@escaping (_ dict: AnyObject?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + NewGetBranchDetail, method: .post, parameters: dictData, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let result = json[kResult] {
                    
                    if let status = json[kstatus] as? String {
                        if status == kOK {
                            completion(result,nil)
                        } else if status == kINVALID {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if kstatus == kFAILED {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if status == kERROR {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                            completion(nil,SLError.EmptyResultError)
                        }
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                    
                }else {
                    if let message = json["message"] as? String {
                        SLAlert.showAlert(str: message)
                    }
                    completion(nil,SLError.EmptyResultError)

                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    //MARK: Get Total Amount
    
    func GetTotalAmt(strFromBranchID :String,strToBranchID :String,strcourierType : String,strkg : String,strUserID:String, completion:@escaping (_ dict: AnyObject?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + NewGetTotalAmt, method: .post, parameters: ["from_branch_id" : strFromBranchID,"to_branch_id" : strToBranchID,"courier_type_id" : strcourierType, "kgs": strkg,"user_id":strUserID],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            
            if let json = response.result.value as? JSONDictionary {
                
                if let result = json[kResult] {
                    
                    if let status = json[kstatus] as? String {
                        if status == kOK {
                            completion(result,nil)
                        } else if status == kINVALID {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if kstatus == kFAILED {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if status == kERROR {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                            completion(nil,SLError.EmptyResultError)
                        }
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                    
                }else {
                    if let message = json["message"] as? String {
                        SLAlert.showAlert(str: message)
                    }
                    
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    
    
    //MARK: GetCustomerDetailUrl
    
    func GetCustomerDetail(strUser :String, completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + CustomerDetailUrl, method: .post, parameters: ["user_id" : strUser],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json["status"] {
                    if status as! String == "success" {
                        if let result = json[kResult] {
                            completion(result as! JSONDictionary ,nil)
                        }else {
                            completion(nil,SLError.EmptyResultError)
                        }
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    //MARK: SaveCustomerDetail
    
    func SaveCustomerDetail(strUser :String,name:String,mobileNo:String,address:String, completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + SaveCustomerDetailUrl, method: .post, parameters: ["user_id" : strUser,"name" : name,"phone" : mobileNo,"address" : address],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json["status"] {
                    if status as! String == "success" {
                        if let result = json[kResult] {
                            completion(result as! JSONDictionary ,nil)
                        }else {
                            completion(nil,SLError.EmptyResultError)
                        }
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }

    //MARK: Get CourierBoy Location
    
    func GetCourierBoyLocation(strUser :String,strorderID :String, completion:@escaping (_ dict: [[String:AnyObject]]?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + CustomerTrackerURL, method: .post, parameters: ["user_id" : strUser,"order_id" : strorderID],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            
            if let json = response.result.value as? JSONDictionary {
                
                if let result = json[kResult] {
                    
                    if let status = json[kstatus] as? String {
                        if status == kOK {
                            completion(result as? [[String : AnyObject]],nil)
                        } else if status == kINVALID {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if kstatus == kFAILED {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if status == kERROR {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                            completion(nil,SLError.EmptyResultError)
                        }
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                    
                }else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }

    //MARK: Get Running Courier
    
    func GetRunningCourier(strUser :String, completion:@escaping (_ dict: [[String:AnyObject]]?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + NewGetrunningCourier, method: .post, parameters: ["user_id" : strUser],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            
            if let json = response.result.value as? JSONDictionary {
                
                if let result = json[kResult] {
                    
                    if let status = json[kstatus] as? String {
                        if status == kOK {
                            completion(result as? [[String : AnyObject]],nil)
                        } else if status == kINVALID {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if kstatus == kFAILED {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if status == kERROR {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                            completion(nil,SLError.EmptyResultError)
                        }
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                    
                }else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    //MARK: Get Done Courier
    func GetDoneCourier(strUser :String, completion:@escaping (_ dict: [[String:AnyObject]]?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + NewGetDoneCourier, method: .post, parameters: ["user_id" : strUser],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let result = json[kResult] {
                    
                    if let status = json[kstatus] as? String {
                        if status == kOK {
                            completion(result as? [[String : AnyObject]],nil)
                        } else if status == kINVALID {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if kstatus == kFAILED {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if status == kERROR {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                            completion(nil,SLError.EmptyResultError)
                        }
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                    
                }else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    //MARK: ForGotPass
    
    func forgotPasswordUser(email: String, completion:@escaping (_ error: NSError?) -> ()) {
        
        Alamofire.request(NewBaseWebRequestUrl + NewForgotPassCourier, method: .post, parameters:[kEmail:email]).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                let status = json[kstatus] as? String
                if status == kOK {
                    if let message = json[kmessage] as? String {
                        SLAlert.showAlert(str: message)
                    }
                    completion(nil)
                } else if status == kINVALID {
                    completion(SLError.EmptyResultError)
                } else if kstatus == kFAILED {
                    completion(SLError.EmptyResultError)
                }else if(status == kERROR){
                    if let message = json[kmessage] as? String {
                        SLAlert.showAlert(str: message)
                        completion(nil)
                    }
                }
                
            } else {
                completion(SLError.EmptyResultError)
            }
        }
        
    }
    //MARK: Get Done Courier
    
    func GetCourierBoyDetils(brinch_id :String,userID:String,fromLat : String,fromLong : String, completion:@escaping (_ dict: [[String:AnyObject]]?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + NewGetCourierboyList, method: .post, parameters: ["branch_id" : brinch_id,"user_id" : userID,"from_latitude":fromLat,"from_longitude": fromLong],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let result = json[kResult] {
                    
                    if let status = json[kstatus] as? String {
                        if status == kOK {
                            completion(result as? [[String : AnyObject]],nil)
                        } else if status == kINVALID {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if kstatus == kFAILED {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if status == kERROR {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                            completion(nil,SLError.EmptyResultError)
                        }
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                    
                }else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    
    //MARK: Add Order
    
    func AddOrder(dictData: JSONDictionary, completion:@escaping (_ dict: AnyObject?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + NewAddOrder, method: .post, parameters: dictData,headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let result = json[kResult] {
                    
                    if let status = json[kstatus] as? String {
                        if status == kOK {
                            completion(result,nil)
                        } else if status == kINVALID {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if kstatus == kFAILED {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            completion(nil,SLError.EmptyResultError)
                        } else if status == kERROR {
                            if let message = json["message"] as? String {
                                SLAlert.showAlert(str: message)
                            }
                            //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                            completion(nil,SLError.EmptyResultError)
                        }
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                    
                }else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    
    //MARK: GetAddress
    func GetAddress(strUser :String, completion:@escaping (_ dict: [[String:AnyObject]]?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + GetAddresses, method: .post, parameters: ["user_id" : strUser],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                
                if let status = json[kstatus] as? String {
                    
                    if status == kOK {
                            if let result = json[kResult] {
                                completion(result as? [[String : AnyObject]],nil)
                            }else {
                                completion(nil,SLError.EmptyResultError)
                            }
                    } else if status == kINVALID {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(nil,SLError.EmptyResultError)
                    } else if kstatus == kFAILED {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(nil,SLError.EmptyResultError)
                    } else if status == kERROR {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
                
                
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    
    
    //MARK: DeleteAddress
    func DeleteAddress(strUser :String,strAddId : String
        , completion:@escaping (_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + DeleteAddresses, method: .post, parameters: ["user_id" : strUser,"address_id" : strAddId],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json[kstatus] as? String {
                    if status == kOK {
                        completion(nil)
                    } else if status == kINVALID {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(SLError.EmptyResultError)
                    } else if kstatus == kFAILED {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(SLError.EmptyResultError)
                    } else if status == kERROR {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                        completion(SLError.EmptyResultError)
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
            } else {
                completion(SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    // MARK: GetOrderHistory
    //func GetAddress(strUser :String, completion:@escaping (_ dict: [[String:AnyObject]]?,_ error: NSError?) -> ())  {

    func GetOrderHistory(strUser :String,strOrdID : String,strBranchID : String
        , completion:@escaping (_ dict: [[String:AnyObject]]?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + OrderHistory, method: .post, parameters: ["user_id" : strUser,"order_id" : strOrdID,"branch_id" : strBranchID],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json[kstatus] as? String {
                    if status == kOK {
                        if let result = json[kResult] {
                            completion(result as? [[String : AnyObject]],nil)
                        }else {
                            completion(nil,SLError.EmptyResultError)
                        }
                    } else if status == kINVALID {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(nil,SLError.EmptyResultError)
                    } else if kstatus == kFAILED {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(nil,SLError.EmptyResultError)
                    } else if status == kERROR {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
   
    //MARK: CancelOrder
    
    func CancelOrder(strUser :String,strOrdID : String,reason:String, completion:@escaping (_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + CancelOrderUrl, method: .post, parameters: ["user_id" : strUser,"order_id" : strOrdID,"cancel_reason":reason],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json["status"] as? String{
                    if status == "success"  {
                            completion(nil)
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
            } else {
                completion(SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }

    
    //MARK: RepeatOrder

    func RepeatOrder(strUser :String,strOrdID : String, completion:@escaping (_ dict: [[String:AnyObject]]?,_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + RepeatOrderUrl, method: .post, parameters: ["user_id" : strUser,"order_id" : strOrdID],headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json["errorCode"] {
                    if "\(status)" == kErrorZero {
                        if let result = json[kResult] {
                            completion(result as? [[String : AnyObject]],nil)
                        }else {
                            completion(nil,SLError.EmptyResultError)
                        }
                    } 
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }

    //MARK: Addaddress
    
    func Addaddress(addressData : AddressNew, completion:@escaping (_ error: NSError?) -> ())  {
        
        Alamofire.request(NewBaseWebRequestUrl + AddAddresses, method: .post, parameters: addressData.toJsonDict(),headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { response in
            
            if let json = response.result.value as? JSONDictionary {
                
                if let status = json[kstatus] as? String {
                    if status == kOK {
                        if let message = json["message"] as? String {
                            //                                SLAlert.showAlert(str: message)
                            completion(nil)
                        }
                    } else if status == kINVALID {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(SLError.EmptyResultError)
                    } else if kstatus == kFAILED {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        completion(SLError.EmptyResultError)
                    } else if status == kERROR {
                        if let message = json["message"] as? String {
                            SLAlert.showAlert(str: message)
                        }
                        //  SLAlert.showAlert(str: LoggedInUser.sharedInstance.message!)
                        completion(SLError.EmptyResultError)
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
                
            } else {
                completion(SLError.EmptyResultError)
                SLAlert.showAlert(str: "something Wrong")
                
            }
            
        }
    }
    
 }
 /*
  
  func forgotPasswordUser(email: String, completion:@escaping (_ error: NSError?) -> ()) {
  
  Alamofire.request(NewBaseWebRequestUrl + NewForgotPasswordUrl, method: .post, parameters:[kEmail:email]).responseJSON { response in
  
  if let json = response.result.value as? JSONDictionary {
  let status = json[kstatus] as? String
  if status == kOK {
  if let message = json[kmessage] as? String {
  SLAlert.showAlert(str: message)
  }
  completion(nil)
  } else if status == kINVALID {
  completion(SLError.EmptyResultError)
  } else if kstatus == kFAILED {
  completion(SLError.EmptyResultError)
  }else if(status == kERROR){
  if let message = json[kmessage] as? String {
  SLAlert.showAlert(str: message)
  completion(nil)
  }
  }
  
  } else {
  completion(SLError.EmptyResultError)
  }
  }
  
  }
  
  
  func logoutUser(completion:@escaping () -> ()) {
  APPDELEGATE.addChargementLoader()
  Alamofire.request(NewBaseWebRequestUrl + kURLLogot, method: .post, parameters:[:]).responseJSON { response in
  APPDELEGATE.removeChargementLoader()
  completion()
  }
  }
  
  func getNotificationCountValue(completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
  
  
  
  completion(nil,SLError.EmptyResultError)
  return
  //        print(LoggedInUser.sharedInstance.getAuth())
  
  
  //
  //            completion(nil,SLError.EmptyResultError)
  //
  //            return
  //            if let json = response.result.value as? JSONDictionary {
  //                //print(json)
  //
  //                if let result = json[kResult] as? JSONDictionary {
  //                    print(result)
  //
  //                    if "\(result[kstatus]!)" == kOK {
  //                        completion(result,nil)
  //                    } else if "\(result[kstatus]!)" == kERROR {
  //                        completion(nil,SLError.EmptyResultError)
  ////                        SLAlert.showAlert(str: "\(result[kmessage]!)")
  //                    } else if "\(result[kstatus]!)" == kINVALID {
  //                        completion(nil,SLError.EmptyResultError)
  ////                        SLAlert.showAlert(str: "\(result[kmessage]!)")
  //                    }
  //
  //                } else {
  //                    completion(nil,SLError.EmptyResultError)
  //                }
  //
  //            } else {
  //                completion(nil,SLError.EmptyResultError)
  //            }
  //        }
  }
  
  //MARK: - requestGetProjectList
  
  func requestGetProjectlist(project: Projects ,completion:@escaping (_ projects:ProjectsResponse?, _ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + GetMyProjects, method: .get, parameters: nil, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  //print(json)
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  
  let projectResponse = ProjectsResponse()
  projectResponse.populateWithJson(dict: json)
  completion(projectResponse, nil)
  } else {
  
  }
  
  
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  }
  
  
  func requestGetMyCurrentProject(project: Projects ,completion:@escaping (_ projects:ProjectsResponse?, _ error: NSError?) -> ()) {
  
  Alamofire.request(NewBaseWebRequestUrl + NewGetMyProjects, method: .post, parameters: project.tojsonDict(), headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  //                //print(json)
  
  let status = json[kstatus] as? String
  
  if status == kOK {
  let projectResponse = ProjectsResponse()
  projectResponse.newpopulateWithJson(dict: json)//newpopulateWithJson(dict: json)
  completion(projectResponse, nil)
  
  } else if status == kINVALID {
  completion(nil, SLError.EmptyResultError)
  } else if kstatus == kFAILED {
  completion(nil, SLError.EmptyResultError)
  }else if(status == kERROR){
  if let message = json[kmessage] as? String {
  SLAlert.showAlert(str: message)
  completion(nil, SLError.EmptyResultError)
  }
  }
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  }
  
  //MARK: - GetAllArchivedProject
  
  func getAllArchivedProject(project: Projects ,completion:@escaping (_ projects:ProjectsResponse?, _ error: NSError?) -> ()) {
  
  
  Alamofire.request(BaseWebRequestUrl + GetAllArchivedProject, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  ////print(json)
  
  if let result = json[kResult] as? JSONDictionary {
  print(result)
  if "\(result[kstatus]!)" == kOK {
  let status = json[kstatus] as? String
  
  if status == kOK {
  let projectResponse = ProjectsResponse()
  projectResponse.populateWithJson(dict: json)
  completion(projectResponse, nil)
  
  } else if status == kINVALID {
  completion(nil, SLError.EmptyResultError)
  } else if kstatus == kFAILED {
  completion(nil, SLError.EmptyResultError)
  }else if(status == kERROR){
  if let message = json[kmessage] as? String {
  SLAlert.showAlert(str: message)
  completion(nil, SLError.EmptyResultError)
  
  }
  
  }
  
  
  } else {
  completion(nil, SLError.EmptyResultError)
  }
  
  } else {
  print(kFAILED)
  completion(nil, SLError.EmptyResultError)
  }
  
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  }
  //MARK: profile
  
  func getUserProfileDetails(completion: @escaping(_ dict: JSONDictionary?, _ error: NSError?) -> ()) {
  
  
  Alamofire.request(NewBaseWebRequestUrl + NewGetMyProfileDetails, method: .post, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  if "\(json[kstatus]!)" == kOK {
  completion(json, nil)
  } else {
  completion(nil, SLError.EmptyResultError)
  }
  
  
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  }
  
  //MARK: Details
  func GetDashboardDetails(projectID: String, completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
  
  Alamofire.request(NewBaseWebRequestUrl + NewProjectDetail, method: .post, parameters: ["project_id":"\(projectID)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  if "\(json[kstatus]!)" == kOK {
  //                    if let message = json[kmessage] as? String {
  //                        SLAlert.showAlert(str: message)
  //                    }
  completion(json, nil)
  
  } else {
  if let message = json[kmessage] as? String {
  SLAlert.showAlert(str: message)
  }
  
  completion(nil, SLError.EmptyResultError)
  }
  
  
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  
  }
  
  //MARK: Dashboard
  
  func GetDashboard(dashboard: Dashboard, completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
  
  Alamofire.request(NewBaseWebRequestUrl + NewProjectDashboard, method: .post, parameters: dashboard.toJasonDict(), headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  if "\(json[kstatus]!)" == kOK {
  completion(json, nil)
  } else if(("\(json[kstatus]!)") == kERROR){
  
  if let message = json[kmessage] as? String {
  SLAlert.showAlert(str: message)
  }
  
  completion(nil, SLError.EmptyResultError)
  }else{
  completion(nil, SLError.EmptyResultError)
  }
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  }
  //MARK: Add Certificate
  func AddCertificate(certificate: Certificate,image : UIImage, completion:@escaping (_ error: NSError?) -> ())  {
  
  Alamofire.upload(multipartFormData:{ multipartFormData in
  
  if let imageData = UIImageJPEGRepresentation(image , 0.6) {
  multipartFormData.append(imageData, withName: "certificate_url", fileName: "file.png", mimeType: "image/png")
  }
  
  for (key, value) in certificate.toJasonDict() {
  
  multipartFormData.append(String.init(value as! NSString).data(using: String.Encoding.utf8)!, withName: key)
  
  }
  
  },
  usingThreshold:UInt64.init(),
  to:NewBaseWebRequestUrl + NewAddCertificate,
  method:.post,
  headers:LoggedInUser.sharedInstance.getAuth(),
  encodingCompletion: { encodingResult in
  switch encodingResult {
  case .success(let upload, _, _):
  upload.responseJSON { response in
  if let json = response.result.value as? JSONDictionary {
  
  let status = json[kstatus] as? String
  
  if status == kOK {
  if let message = json[kmessage] as? String {
  SLAlert.showAlert(str: message)
  }
  
  completion(nil)
  
  } else if status == kINVALID {
  completion(SLError.EmptyResultError)
  } else if kstatus == kFAILED {
  completion(SLError.EmptyResultError)
  }else if(status == kERROR){
  if let message = json[kmessage] as? String {
  SLAlert.showAlert(str: message)
  completion(nil)
  
  }
  
  }
  
  
  } else {
  completion(SLError.EmptyResultError)
  }
  
  }
  case .failure(let encodingError):
  completion(SLError.EmptyResultError)
  }
  })
  
  }
  
  
  //MARK: Update Profile
  
  
  func updateUserProfile(profile: Profile, completion: @escaping(_ error:NSError?) -> ()) {
  
  
  Alamofire.upload(multipartFormData:{ multipartFormData in
  
  let image = profile.profileImage
  if image != nil {
  if let imageData = UIImageJPEGRepresentation(image!, 0.6) {
  multipartFormData.append(imageData, withName: "avatar", fileName: "avatar.png", mimeType: "image/png")
  }
  }
  
  for (key, value) in profile.toJasonDict() {
  
  multipartFormData.append(String.init(value as! NSString).data(using: String.Encoding.utf8)!, withName: key)
  
  }
  
  },
  usingThreshold:UInt64.init(),
  to:BaseWebRequestUrl + SaveMyProfileURL,
  method:.post,
  headers:LoggedInUser.sharedInstance.getAuth(),
  encodingCompletion: { encodingResult in
  switch encodingResult {
  case .success(let upload, _, _):
  upload.responseJSON { response in
  if let json = response.result.value as? JSONDictionary {
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  completion(nil)
  } else {
  completion(SLError.EmptyResultError)
  }
  } else {
  completion(SLError.EmptyResultError)
  }
  } else {
  completion(SLError.EmptyResultError)
  }
  
  }
  case .failure(let encodingError):
  completion(SLError.EmptyResultError)
  }
  })
  
  //        Alamofire.upload(.POST, BaseWebRequestUrl + SaveMyProfileURL, multipartFormData: {
  //            multipartFormData in
  //
  //            if image != nil {
  //                if let imageData = UIImageJPEGRepresentation(image!, 0.6) {
  //                    multipartFormData.appendBodyPart(data: imageData, name: "profilePicture", fileName: "file.png", mimeType: "image/png")
  //                }
  //            }
  //
  //            for (key, value) in register.toJsonDictionary() {
  //                multipartFormData.appendBodyPart(data: value.data(using: String.Encoding.utf8)!, name: key)
  //
  //            }
  //        }, encodingCompletion: {
  //            encodingResult in
  //
  //            switch encodingResult {
  //            case .success(let upload, _, _):
  //
  //                upload.responseJSON {
  //                    response in
  //
  //                    print(response.result.value)
  //                    if let dict = (response.result.value as? JSONDictionary), let statusCode = dict[kSP_StatusCode] as? Int {
  //
  //                        if statusCode == 200 {
  //                            completion()
  //                            ShowAlert(dict[kSP_Success]![kSP_Message]! as! String)
  //
  //                        } else {
  //                            ShowAlert(dict[kSP_Fail]![kSP_Message]! as! String)
  //                            failure(error: SPErrors.EmptyResultError)
  //                        }
  //
  //                    } else {
  //                        failure(error: SPErrors.EmptyResultError)
  //                    }
  //
  //                }
  //
  //            case .failure(let encodingError):
  //                print(encodingError)
  //                failure(error: SPErrors.EmptyResultError)
  //            }
  //
  //
  //        })
  
  
  
  
  //        Alamofire.request(BaseWebRequestUrl + SaveMyProfileURL, method: .post, parameters: profile.toJasonDict(), headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  //
  //            if let json = response.result.value as? JSONDictionary {
  //                if let result = json[kResult] as? JSONDictionary {
  //
  //                    if "\(result[kstatus]!)" == kOK {
  //                        completion(nil)
  //                    } else {
  //                        completion(SLError.EmptyResultError)
  //                    }
  //                } else {
  //                    completion(SLError.EmptyResultError)
  //                }
  //            } else {
  //                completion(SLError.EmptyResultError)
  //            }
  //        }
  }
  
  func getAccountsBillingInfo(completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + GetAccountBillingInfo, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  //print(json)
  if let result = json[kResult] as? JSONDictionary {
  print(result)
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  
  }
  
  //MARK:- ChekIn
  
  func doChekIn(project_id: String, checkInType:Bool, completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
  //        print(["project_id":"\(project_id)","auto":"\(auto)"])
  let parameter = ["project_id":"\(project_id)","auto":"\(checkInType ? "1" : "0")"]
  Alamofire.request(NewBaseWebRequestUrl + newDocheckIn, method: .post, parameters: parameter, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  LocationClass.sharedInstance.setLog(str: "\(json)")
  
  if "\(json[kstatus]!)" == kOK {
  completion(json, nil)
  
  } else {
  completion(nil, SLError.EmptyResultError)
  }
  
  
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  
  }
  
  
  //MARK:- checkOut
  func doChekout(project_id: String, checkOutType:Bool, completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
  //        print(["project_id":"\(project_id)","auto":"\(auto ? "1" : "0")"])
  
  let parameter = ["project_id":"\(project_id)","auto":"\(checkOutType ? "1" : "0")"]
  LocationClass.sharedInstance.setLog(str: "\(parameter)")
  Alamofire.request(NewBaseWebRequestUrl + newDocheckout, method: .post, parameters: parameter, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  LocationClass.sharedInstance.setLog(str: "\(json)")
  if "\(json[kstatus]!)" == kOK {
  completion(json, nil)
  } else {
  completion(nil, SLError.EmptyResultError)
  }
  
  
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  
  }
  
  
  //MARK:- GetProjectReportListURL
  
  func GetReportData(role: String, completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
  
  Alamofire.request(NewBaseWebRequestUrl + NewReportlistdata, method: .post, parameters: ["role":"\(role)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  if "\(json[kstatus]!)" == kOK {
  completion(json, nil)
  } else {
  completion(nil, SLError.EmptyResultError)
  }
  
  
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  
  }
  func GetReportDetailData(project_id: String,end_date : String, completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {
  
  Alamofire.request(NewBaseWebRequestUrl + NewReportDetaildata, method: .post, parameters: ["project_id":"\(project_id)","end_date":"\(end_date)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  if "\(json[kstatus]!)" == kOK {
  completion(json, nil)
  } else {
  completion(nil, SLError.EmptyResultError)
  }
  
  
  } else {
  completion(nil, SLError.EmptyResultError)
  print(kFAILED)
  }
  }
  
  }
  
  
  func fetchAllProjectsReportList(completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + GetProjectReportListURL, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  
  completion(json,nil)
  
  
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func fetchAllCrewReportList(completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + GetCrewReportListURL, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  
  completion(json,nil)
  
  
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  
  func fetchgoogleData(text: String,completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ())  {
  //[NSString stringWithFormat:@"%@input=%@&types=geocode&sensor=false&key=%@",GOOGLE_AUTOCOMPLETE_SEARCH_URL,textFieldText,kGOOGLE_MAP_SERVER_API_KEY_19JUNE]
  let strURL = "\(GOOGLE_AUTOCOMPLETE_SEARCH_URL)input=\(text)&types=geocode&sensor=false&key=\(kGOOGLE_MAP_SERVER_API_KEY_19JUNE)"
  
  Alamofire.request(strURL, method: .get).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func getCompleteData(text: String,completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ())  {
  
  let strURL = "\(kGOOGLE_GEOCODE_URL)=\(text)&sensor=false"
  
  Alamofire.request(strURL, method: .get).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }        }
  }
  
  func createNewProject(proje: [String:String],completion:@escaping (_ dict:JSONDictionary?, _ error: NSError?) -> ()) {
  
  print(LoggedInUser.sharedInstance.getAuth())
  print(proje)
  Alamofire.request(BaseWebRequestUrl + CreateProjectUrl, method: .post, parameters: proje, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  print(response.result.value)
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func getGoogleData(url:String, completion:@escaping(_ dict:JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(url, method: .get).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  //print(json)
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  
  }
  
  
  func getProjectDetails(projectID: String,completion:@escaping (_ dict:JSONDictionary?, _ error: NSError?) -> ()) {
  
  
  print(BaseWebRequestUrl + GetProjectDetails + "\(projectID).json")
  
  Alamofire.request(BaseWebRequestUrl + GetProjectDetails + "\(projectID).json", method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  print(response.result.value)
  
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func getProjectNotes(projectId: String, completion:@escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + GetProjectNotesURL + "\(projectId).json", method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  
  func uploadAlbumImages(dict: JSONDictionary, completion:@escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + SaveNotesURL, method: .post, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  //print(json)
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  
  func deleteProjectWebSerrviceCalling(projectID: String,completion: @escaping (_ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + DeleteProjectURL + "\(projectID).json", method: .delete, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  
  //print(json)
  completion(nil)
  } else {
  completion(SLError.EmptyResultError)
  }
  } else {
  completion(SLError.EmptyResultError)
  }
  } else {
  completion(SLError.EmptyResultError)
  }
  }
  }
  
  func makeProjectArchiveWebSirvice(projectID: String,completion: @escaping (_ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + MakeProjectArchiveURL + "\(projectID).json", method: .put, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  
  //print(json)
  completion(nil)
  } else {
  completion(SLError.EmptyResultError)
  }
  } else {
  completion(SLError.EmptyResultError)
  }
  } else {
  completion(SLError.EmptyResultError)
  }
  }
  }
  
  func disableCrewNotificationWebService(dict: JSONDictionary, completion: @escaping(_ dict: JSONDictionary?,_ eror: NSError?) -> ()) {
  
  print(dict)
  
  Alamofire.request(BaseWebRequestUrl + DisableCrewNotificationsUrl, method: .put, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  
  //print(json)
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func searchCrewForAddingToProject(projectID: String, completion: @escaping(_ dict: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + GetSearchedCrewForProject, method: .post, parameters: ["project_id":"\(projectID)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  
  //print(json)
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func addCrewIntoProject(dict: JSONDictionary, completion: @escaping(_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + GetSearchedCrewForProject, method: .post, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  
  //print(json)
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func addCrew(dict:[String: String], completion: @escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + AddCrewBySelection, method: .post, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func deleteCrewMemberWeservice(dict: JSONDictionary, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  Alamofire.request(BaseWebRequestUrl + DeleteCrewMemberURL, method: .post, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  //print(json)
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func getPayPalRequiredAttributes(completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  
  Alamofire.request(BaseWebRequestUrl + GetPayPalAttributes, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  //print(json)
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func storeCreditCard(cardDict: JSONDictionary, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + StoreCreditCard, method: .post, parameters:cardDict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  
  print(response.result.value)
  
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  //print(json)
  
  if "\(result[kstatus]!)" == kOK {
  completion(result,nil)
  } else if "\(result[kstatus]!)" == kERROR {
  completion(nil,SLError.EmptyResultError)
  SLAlert.showAlert(str: "\(result[kmessage]!)")
  } else if "\(result[kstatus]!)" == kINVALID {
  completion(nil,SLError.EmptyResultError)
  SLAlert.showAlert(str: "\(result[kmessage]!)")
  }
  
  
  } else {
  
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func sendInvitationViaEmail(email: String, projectID: String, completion: @escaping(_ error: NSError?) -> ()) {
  Alamofire.request(BaseWebRequestUrl + InviteCrewViaEmail, method: .post, parameters:["project_id":"\(projectID)", "email":"\(email)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  print(response.result.value)
  if let json = response.result.value as? JSONDictionary {
  if let result = json[kResult] as? JSONDictionary {
  //print(json)
  
  if "\(result[kstatus]!)" == kOK {
  completion(nil)
  } else if "\(result[kstatus]!)" == kERROR {
  completion(SLError.EmptyResultError)
  SLAlert.showAlert(str: "\(result[kmessage]!)")
  } else if "\(result[kstatus]!)" == kINVALID {
  completion(SLError.EmptyResultError)
  SLAlert.showAlert(str: "\(result[kmessage]!)")
  }
  } else {
  completion(SLError.EmptyResultError)
  }
  } else {
  completion(SLError.EmptyResultError)
  }
  }
  
  }
  
  //MARK: - Appdelege methods
  //MARK: - ------------
  
  func requestGetMyCurrentProject(completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  
  Alamofire.request(BaseWebRequestUrl + "getMyProjects.json?role_id=4&page=1", method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  
  if let result = json[kResult] as? JSONDictionary {
  
  if "\(result[kstatus]!)" == kOK {
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  
  
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func getManualCheckInProjectsArray() {
  
  
  }
  
  func getAllNotificationData(completion: @escaping (_ notificaitons: [Notifications]?,_ error: NSError?) -> ()) {
  
  
  Alamofire.request(NewBaseWebRequestUrl + GetAllNotifications, method: .post, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  if let result = json[kData] as? [JSONDictionary] {
  var notificatoinArray = [Notifications]()
  for notificatonObj in result {
  let notificaiton = Notifications()
  notificaiton.parseJsonDict(dic: notificatonObj)
  notificatoinArray.append(notificaiton)
  }
  completion(notificatoinArray,nil)
  } else {
  if "\(json[kstatus]!)" == kOK {
  completion([],nil)
  } else if "\(json[kstatus]!)" == kERROR {
  completion(nil,SLError.EmptyResultError)
  } else if "\(json[kstatus]!)" == kINVALID {
  completion(nil,SLError.EmptyResultError)
  }
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func checkInProject(dict: JSONDictionary, completion: @escaping(_ result: JSONDictionary?, _ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + CheckInUrl, method: .get, parameters:dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  
  if let result = json[kResult] as? JSONDictionary {
  print(result)
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  
  func acceptProjectWebService(dict: JSONDictionary, completion: @escaping(_ result: JSONDictionary?, _ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + AcceptProjectURL, method: .post, parameters:dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  //print(json)
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func checkOutProject(dict: JSONDictionary, completion: @escaping(_ result: JSONDictionary?, _ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + CheckOutUrl, method: .get, parameters:dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  
  func notificationReadWebService(id: String, completion: @escaping (_ completion: Bool?,_ error: NSError?) -> ()) {
  
  Alamofire.request(NewBaseWebRequestUrl + MarkNotificationAsReadURL, method: .post, parameters:["role":"crew","notification_id":id], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  if let result = json[kstatus] as? String {
  if result == kOK {
  completion(true,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func callMarkAsReadAllNotification(id: String, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + MarkAllNotificationAsReadUrl, method: .post, parameters:["push_notification_id":"\(id)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  if let result = json[kResult] as? JSONDictionary {
  if "\(result[kstatus]!)" == kOK {
  completion(result,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func fetchNotificationData(id: String, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(BaseWebRequestUrl + TentetativeCheckInNotificationUrl + "/\(id).json", method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  //print(json)
  
  //                if let result = json[kResult] as? JSONDictionary {
  //
  //                    if "\(result[kstatus]!)" == kOK {
  completion(json,nil)
  //                    } else {
  //                        completion(nil,SLError.EmptyResultError)
  //                    }
  //
  //
  //                } else {
  //                    completion(nil,SLError.EmptyResultError)
  //                }
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  
  }
  func callAcceptTentativeCheckInWebService(url: String, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(url, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  //print(json)
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func callWebService(url: String, method:HTTPMethod, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(url, method: method, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  //print(json)
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  
  func callWebServiceWithParameter(url: String, dict: [String:String]?,method:HTTPMethod, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
  
  Alamofire.request(url, method: method, parameters:dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
  
  if let json = response.result.value as? JSONDictionary {
  
  completion(json,nil)
  } else {
  completion(nil,SLError.EmptyResultError)
  }
  }
  }
  }
  */
 
 
