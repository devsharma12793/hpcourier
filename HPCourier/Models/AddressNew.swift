//
//  AddressNew.swift
//  HPCourier
//
//  Created by Devendra on 15/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import Foundation
class AddressNew{
    
    var user_id : String?
    var name : String?
    var number : String?
    var address : String?
    var latitude : String?
    var longitude : String?
    
    func populateWithJson(dict: JSONDictionary) {
        
        if let user_id = dict["user_id"] as? String{
            self.user_id = user_id
        }

        if let name = dict[kname] as? String{
            self.name = name
        }
        if let Mobile = dict[kRMobile] as? String{
            self.number = Mobile
        }
        if let address = dict["address"] as? String{
            self.address = address
        }
        if let lat = dict["latitude"] as? String{
            self.latitude = lat
        }
        if let long = dict["longitude"] as? String{
            self.longitude = long
        }
        
        
    }
    func toJsonDict() -> JSONDictionary {
        
        var dict: JSONDictionary = [:]
        
        if let user_id = user_id { dict["user_id"] = user_id as AnyObject? }

        if let name = name { dict["name"] = name as AnyObject? }
        
        if let number = number { dict["number"] = number as AnyObject? }
        
        if let address = address { dict["address"] = address as AnyObject? }
        
        if let latitude = latitude { dict["latitude"] = latitude as AnyObject? }
        
        if let longitude = longitude { dict["longitude"] = longitude as AnyObject? }
        
        return dict
}
}
