//
//  Address.swift
//  HPCourier
//
//  Created by Devendra on 04/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import Foundation

class AddressDict{
   
    var name : String?
    var Mobile : String?
    var addressString : String?
    var streetNo : String?
    var Street : String?
    var pincode : String?
    var latitude : String?
    var longitude : String?
    var city : String?
    var state : String?
    var country : String?

    func populateWithJson(dict: Address) {
    
        if let name = dict.name{
            self.name = name
        }
        if let Mobile = dict.mobile{
            self.Mobile = Mobile
        }
        if let addressString = dict.addressString{
            self.addressString = addressString
        }
        if let streetNo = dict.streetNo{
            self.streetNo = streetNo
        }
        if let Street = dict.Street{
            self.Street = Street
        }
        if let pincode = dict.pincode{
            self.pincode = pincode
        }
        if let lat = dict.latitute{
            self.latitude = lat
        }
        if let long = dict.longitude{
            self.longitude = long
        }
        if let city = dict.city{
            self.city = city
        }
        if let state = dict.state{
            self.state = state
        }
        if let country = dict.country{
            self.country = country
        }

        
    }
    func toJsonDict() -> JSONDictionary {
      
        var dict: JSONDictionary = [:]
        
        if let name = name { dict["name"] = name as AnyObject? }
        
        if let mobile = Mobile { dict["Mobile"] = mobile as AnyObject? }

        if let addressString = addressString { dict["addressString"] = addressString as AnyObject? }
        
        if let streetNo = streetNo { dict["streetNo"] = streetNo as AnyObject? }
        
        if let Street = Street { dict["Street"] = Street as AnyObject? }
        
        if let pincode = pincode { dict["pincode"] = pincode as AnyObject? }
     
        if let latitude = latitude { dict["latitude"] = latitude as AnyObject? }
        
        if let longitude = longitude { dict["longitude"] = longitude as AnyObject? }

        if let city = city { dict["city"] = city as AnyObject? }
        
        if let state = state { dict["state"] = state as AnyObject? }
        
        if let country = pincode { dict["country"] = country as AnyObject? }

        return dict
    }

    
}
