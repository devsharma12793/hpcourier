//
//  Register.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation
public typealias JSONDictionary = [String: AnyObject]

class Register {

    var name: String?
    var email: String?
    var phone: String?
    var password: String?
    var c_password: String?
    var device_token: String?
//    var terms: String?
//    var user_timezone: String?
//    var devicetoken: String?
//    var role: Int?

    func toJsonDict() -> JSONDictionary {

        var dict: JSONDictionary = [:]

        if let name = name { dict[kname] = name as AnyObject? }
        if let email = email { dict[kemail] = email as AnyObject? }
        if let phone = phone { dict[kphone] = phone as AnyObject? }
        if let password = password { dict[kpassword] = password as AnyObject? }
        if let c_password = c_password { dict[kcpassword] = c_password as AnyObject? }
        if let device_token = device_token { dict[kdevicetoken] = device_token as AnyObject? }

        return dict

    }

}
