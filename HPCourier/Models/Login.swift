//
//  Login.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

class Login {

    var deviceToken: String?
    var social_number: String?
    var name: String?
    var social_type: String?
    var social_email: String?
    var email: String?
    var password: String?
    var login_type: String?

    func toJsonDict() -> JSONDictionary {
        var dict: JSONDictionary = [:]

        if let deviceToken = deviceToken { dict[kdevicetoken] = deviceToken as AnyObject? }

        if let email = email { dict[kEmail] = email as AnyObject? }

        if let password = password { dict[kPassword] = password as AnyObject? }
     
       // if let login_type = login_type { dict[kLoginType] = login_type as AnyObject? }

        return dict
    }
  
    func toJsonDictForSocial() -> JSONDictionary {
        var dict: JSONDictionary = [:]
        
        if let deviceToken = deviceToken { dict[kdevicetoken] = deviceToken as AnyObject? }
             
        if let social_number = social_number { dict["social_number"] = social_number as AnyObject? }

        if let social_email = social_email { dict["email"] = social_email as AnyObject? }
     
        if let social_name = name { dict["name"] = social_name as AnyObject? }

        
        if let social_type = social_type { dict["social_type"] = social_type as AnyObject? }
        
        return dict
    }

}

enum UserType: Int {
    case customer = 4
    case Courierboy = 5
}

public class LoggedInUser {

    var token: String?
    var email: String?
    var name: String?
    var phone: String?
    var id: Int?

    public class var sharedInstance: LoggedInUser {
        struct Singleton {
            static let instance: LoggedInUser = LoggedInUser()
        }
        return Singleton.instance
    }
    init() {

        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                self.populateWithJson(dict: user)
            }
        }
    }
    
    func saveUserData(user: JSONDictionary) {
        let userData = NSKeyedArchiver.archivedData(withRootObject: user)
        UserDefaults.standard.set(userData, forKey: kAPPUser)
    }
    
    func removeUserData() {
        let userData = NSKeyedArchiver.archivedData(withRootObject: JSONDictionary())
        UserDefaults.standard.set(userData, forKey: kAPPUser)
        self.token = nil
    }


    func populateWithJson(dict: JSONDictionary) {
        
//        var token: String?
//        var email: String?
//        var name: String?
//        var phone: String?
//        var id: Int?

        if let token = dict[k_token] as? String {
            self.token = token
        }
        if let email = dict[kemail] as? String {
            self.email = email
        }
        if let name = dict[kname] as? String {
            self.name = name
        }
        if let phone = dict[kphone] as? String {
            self.phone = phone
        }
        if let id = dict[kphone] as? Int {
            self.id = id
        }

    }

    func toJsonDict() -> JSONDictionary {

        var dic: JSONDictionary = [:]
        if let token = token { dic[k_token] = token as AnyObject? }
        return dic
    }

    func getAuth() -> [String:String] {
        
        if self.token == nil {
            return ["":""]
        }
        let authValue = "\(self.token!)"
        
        print(authValue)
        return ["auth-token": "\(authValue)"]
    }
/*
    func authValue() -> String {
        if self.userName == nil {
            return ""
        }
        if self.token == nil {
            return ""
        }

        let authStr = "\(self.userName!):\(self.token!)".data(using: .utf8)
        print(authStr)
        return "Basic \(authStr!.base64EncodedString(options: .endLineWithCarriageReturn))"
    }
*/
}
