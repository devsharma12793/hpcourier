//
//  Constant.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

//let NEWDESIGN_STORYBOARD = UIStoryboard.init(name: "NewDesign", bundle: nil)
//let NEWDESIGN_STORYBOARDFORPixelPerfect = UIStoryboard.init(name: "NewPixelPerfectUI", bundle: nil)


let kMAIN_URL = "http://54.68.29.83/mobile/v1/"
let kGOOGLE_MAP_API_KEY  = "AIzaSyAIIz6J-PHkRyb5mtjHa6peEdv4Yil3bMM"
let kGOOGLE_MAP_SERVER_API_KEY = "AIzaSyCXD3ot9EPY_OLWpDiU7CoTh-7AgbRwR-A"

//let BaseWebRequestUrl = "http://54.68.29.83/mobile/v1/"
let NewBaseWebRequestUrl = "http://13.127.141.71/api/api"
//let NewBaseWebRequestUrl = "http://clonemarkets.com/hpcourier/api"

let ImageBaseUrl = "http://54.68.29.83"

let NewLoginRequestUrl = "/login"
let NewSignupRequestUrl = "/signup"
let CustomerDetailUrl = "/customer_detail"
let SaveCustomerDetailUrl = "/customer_edit_profile"
let NewGetBranchDetail = "/get_branch_detail"
let NewGetTotalAmt = "/get_total_amount"
let NewGetrunningCourier = "/get_running_courier"
let CustomerTrackerURL = "/customer_tracker"

let NewGetDoneCourier = "/get_complete_courier"
let NewForgotPassCourier = "/get_complete_courier"
let NewGetCourierboyList = "/courier_boys"
let NewAddOrder = "/add_order"
let GetAddresses = "/customer_get_address"
let RepeatOrderUrl = "/repeat_order"
let CancelOrderUrl = "/set_cancell_order"
let OrderHistory = "/get_order_history"
let DeleteAddresses = "/customer_delete_address"
let AddAddresses = "/customer_add_address"


//api/get_total_amount
//get_running_courier
//http://clonemarkets.com/hpcourier/api/get_branch_detail

let NewGetMyProfileDetails = "users/updateprofile"
let NewProjectDashboard = "projects/dashboard"
let NewProjectDetail = "projects/detail"
let NewReportlistdata = "reports/list"
let NewReportDetaildata = "reports/detail"
let newDocheckIn = "projects/checkin"
let newDocheckout = "projects/checkout"
let GetAllNotifications = "notifications/getNotificationList"
let MarkNotificationAsReadURL = "notifications/readnotification"
let kURLLogot       = "users/signout"

let LoginRequestUrl = "signIn.json"
let GetMyProjects = "getMyProjects.json"
let NewGetMyProjects = "projects/projectlist"
let NewAddCertificate = "users/addcertification"

let SignupRequestUrl = "signUp.json"
let ForgotPasswordUrl = "getPassword.json"

let NewForgotPasswordUrl = "users/forgetpassword"

let CheckEmailVerificationUrl = "checkEmailVerification.json"
let ResendEmailVerificationLink = "resendEmailVerification.json"
let GetMyProfileDetails = "getMyProfile.json"
let SaveMyProfileURL = "saveProfile.json"
let DeleteCrewMemberURL = "deleteCrewMember.json"
let CreateProjectUrl = "createProject.json"
let GetProjectDetails = "getProjectDetails/"
let GetAllArchivedProject = "getAllMyArchivedProjects.json?"
let GetProjectNotesURL = "getNotes/"
let UploadImageUrl = "imageURL"
let GetNoteDetailsURL = "getNoteDetail/"
let SaveNotesURL = "saveNote.json"
let DeleteProjectURL = "deleteProject/"
let MakeProjectArchiveURL = "makeArchive/"
let DisableCrewNotificationsUrl = "settingPushNotification.json"
let GetUnreadNotificationCount = "countUnreadNotifications.json"

let MarkAllNotificationAsReadUrl = "markReadAllNotifications.json"
let GetSearchedCrewForProject = "searchCrews.json"
let AddCrewBySelection = "sendInvite.json"
let InviteCrewViaEmail = "sendInviteEmail.json"

//MARK: Login --
let kAPPNAME        = "Hp Courier"
let kAPPUser        = "LoggedInUser"
let kProjectData        = "ProjectData"

let kResult         = "result"
let kEmail          = "email"
let kRMobile          = "number"

let kLoginType       = "login_type"

let kPassword       = "password"
let k_token         = "auth_token"
let k_username      = "name"
let kmessage        = "message"
let krole           = "role"
let kstatus         = "status"
let kINVALID        = "INVALID"
let kFAILED         = "FAILED"
let kOK             = "success"
let kErrorZero             = "0"

let kunOrthenticated = 999
let kERROR          = "error"
let kUnread         = "unread"
let kRead           = "read"

//MARK: Register - 
let kid               = "id"
let kname               = "name"
let kemail              = "email"
let kphone              = "phone"
let kpassword          = "password"
let kcpassword          = "c_password"
let kterms_conditions   = "terms_conditions"
let kdevicetoken   = "device_token"

let krole_id        = "role_id"
let kpage           = "page"

let kcount          = "count"
let kprojects       = "projects"
let kData           = "data"
let k0              = "0"
let kcount_crews        = "count_crews"
let kCheckeOutCrew      = "CheckeOutCrew"
let isCheckedin         = "isCheckedIn"
let ktotal              = "total"
let kCheckedInCrew      = "CheckedInCrew"
let kProject            = "Project"
let kCreated            = "created"
let kID                 = "id"
let kmodified           = "modified"
let kproject_city       = "project_city"
let kproject_country    = "project_country"
let kproject_lat        = "project_lat"
let kproject_lng        = "project_lng"
let kproject_location   = "project_location"
let kproject_name       = "project_name"
let kproject_note       = "project_note"
let kproject_postal_code    = "project_postal_code"
let kproject_radius     = "project_radius"
let kproject_state      = "project_state"
let kproject_status     = "project_status"
let kproject_street     = "project_street"
let kproject_street_number  = "project_street_number"
let kproject_timezone    = "project_timezone"
let kuser_id            = "user_id"
let kAddressDataLocal    = "addressdata"
let kfrom_address    = "from_address"
let kfrom_latitude = "from_latitude"
let kfrom_longitude = "from_longitude"
let kto_address = "to_address"
let kto_latitude = "to_latitude"
let kto_longitude = "to_longitude"

struct Address {
    var name : String?
    var mobile : String?
    var addressString : String?
    var streetNo : String?
    var Street : String?
    var pincode : String?
    var latitute : String?
    var longitude : String?
    var city : String?
    var state : String?
    var country : String?


}
var contacts: [Address] = []

