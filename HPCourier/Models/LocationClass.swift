//
//  LocationClass.swift
//  Beehive
//
//  Created by Ankit on 2/1/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

class LocationClass: NSObject, CLLocationManagerDelegate,UNUserNotificationCenterDelegate {

    var locationManager: CLLocationManager!
    var previousLocation: CLLocation?
    var currentLocation: CLLocation?
    var selectedLocation: CLLocation?
    var checkIn: Bool? = false
    var center = UNUserNotificationCenter.current()
    var strProjectDetail: String?
    var strDistance: String? = ""
    var batteryLevel: Float {
        return UIDevice.current.batteryLevel * 100
    }

    var isManualCheckin = false
    
    var munites: Int = 1
    var extraMeter: Int = 50
    
    var horizontalAccuacy: Double = 100
    
    var checkOutCount = 0
    var checkInCount = 0
    var checkOutlocationDict = [Any]()
    var checkInlocationDict = [Any]()
    var lastCheckoutDate = Date.init()
    var lastCheckInDate = Date.init()
    
    var inTimer: Timer?
    var locationUpdateTimer: Timer?
    var chekoutPress: Bool?
    var distanceFilter: CLLocationDistance = 0
    var sucessCall: (() -> Void)?
    
    var i = 0
    var isPopupOpen: Bool = false

    
    public class var sharedInstance: LocationClass {
        struct Singleton {
            static let instance: LocationClass = LocationClass()
        }
        return Singleton.instance
    }
    
    func getMunits(munits: Int) -> Int {
        return munits * 60
    }
    

    override init() {
        super.init()
        
        isManualCheckin = UserDefaults.standard.bool(forKey: "isManualCheckin")
        
        if let munite = UserDefaults.standard.object(forKey: "munites") as? Int {
            munites = munite
        }
        
        if let date = UserDefaults.standard.object(forKey: "lastCheckoutDate") as? Date {
            lastCheckoutDate = date
        }
        
        let options: UNAuthorizationOptions = [.alert, .sound, .badge];
        center.delegate = self
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }
    }
    
    func timerUpdate() {
        let strTimer = "\nUpdate Log Timer---\(APPDELEGATE.getDate())---\n"
        //self.setLog(str: strTimer)
        print(strTimer)
    }
    
    @objc func initLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = distanceFilter
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.requestAlwaysAuthorization()
        DispatchQueue.main.async {
            
        }
        
    }
    
    func startMonitoring() {
//      self.setNotification(body: "update location manually", title: "Location")
//        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.startUpdatingLocation()
//        startReceivingSignificantLocationChanges()
    }
    
    func stopMonitoring() {
//        self.locationManager.stopMonitoringSignificantLocationChanges()
        self.locationManager.stopUpdatingLocation()
//        stopReceivingSignificantLocationChanges()
    }

    
    func setManualCheckin(checkin: Bool) {
        self.isManualCheckin = checkin
        UserDefaults.standard.set(checkin, forKey: "isManualCheckin")
        UserDefaults.standard.synchronize()
    }
    
    
    // MARK: ------------ method ------------

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        var locationUpdateString = ""
        let location = locations.last
        self.currentLocation = location
        let currentLat = location!.coordinate.latitude
        let currentLong = location!.coordinate.longitude
        let currentLocationHorizontalAcc = location!.horizontalAccuracy
        
        if (locationUpdateTimer != nil)
        {
            locationUpdateTimer?.invalidate()
            locationUpdateTimer = nil
        }
        
        locationUpdateTimer = Timer.scheduledTimer(timeInterval: TimeInterval.init(60), target: self, selector: #selector(self.timerLocationUpdate), userInfo: nil, repeats: true)
        
            _ = location!.timestamp
            locationUpdateString = "lat: \(currentLat) \nLong: \(currentLong)\n Speed: \(location!.speed)\n"
            var totalDistance = 0.0
        if isActiveState() {
            NotificationCenter.default.post(name: NSNotification.Name.init("setText"), object: nil)
        }
    }
    
    func isActiveState() -> Bool {
        return UIApplication.shared.applicationState == .active
    }
    
    @objc func timerLocationUpdate() {
       // self.setLog(str: "Location update after 3 mins")
        self.restartMonitoring()
    }
    
    func timerLocationUpdateAfterFirstCheckin() {
        //self.setLog(str: "timerLocationUpdateAfterFirstCheckin 1 min")
        self.restartMonitoring()
    }
    
    func timerLocationUpdateAfterFirstCheckOut() {
      //  self.setLog(str: "timerLocationUpdateAfterFirstCheckOut 1 min")
        self.restartMonitoring()
    }
    
    func showChekinoutAlert() {
        
        if chekoutPress != nil {
            if chekoutPress == true {
                self.showAlert(str: "You are already check in to project area")
            } else {
                self.showAlert(str: "You are already check out from project area")
            }
            chekoutPress = nil
        }
        sucessCall!()
    }
    

    func restartMonitoring() {
        self.stopMonitoring()
        self.startMonitoring()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {}
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {}
    
    func setNotification(body: String, title: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                        repeats: false)
        // Swift
        let identifier = "UYLLocalNotification"
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if error != nil {}
        })
    }
    
    func getTimeStepString() -> String {
        
        let dateFormat = DateFormatter()
        dateFormat.locale = Locale.init(identifier: "en_US")
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormat.string(from: Date.init())
    }
    
    func getTimeStepStringFromDate(date: Date) -> String {
        
        let dateFormat = DateFormatter()
        dateFormat.locale = Locale.init(identifier: "en_US")
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormat.string(from: date)
    }
    
    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        print("\(String(describing: error))")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\(error)")
    }
    
    func showAlert(str: String) {
        
        UIAlertView.init(title: kAPPNAME, message: "\(str)", delegate: nil, cancelButtonTitle: "OK").show()
        
//        let alert = UIAlertController(title: "Alert", message: "\(str)", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    

    func seconds(from date: Date) -> Int {
        
        let unitFlags = Set<Calendar.Component>([.second])
        return Calendar.current.dateComponents(unitFlags, from: date, to: Date.init()).second ?? 0
    }
    
    
    func saveTextFile(string: String?)
    {
        
        if string == nil{
            return
        }
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let basePath = ((paths.count) > 0) ? "\(paths[0])" : ""
        let fileDestinationUrl = (basePath as NSString).appendingPathComponent("file.txt") as NSString?
        //        let text = LOGSTRING
        
        if !FileManager.default.fileExists(atPath: fileDestinationUrl! as String){
            do {
                try string!.write(toFile: fileDestinationUrl! as String, atomically: false, encoding: .utf32)
            } catch let error as NSError {
                print("error writing to url \(String(describing: fileDestinationUrl))")
                print(error.localizedDescription)
            }
            return
        }
        
        
        do {
            let filehandel = try FileHandle.init(forWritingTo: URL.init(fileURLWithPath: fileDestinationUrl! as String))
            filehandel.seekToEndOfFile()
            filehandel.write(string!.data(using: String.Encoding.utf32)!)
        } catch let error {
            print(error)
        }
        
        
    }
    func setupLogFile() -> String? {
        //        if let str = UserDefaults.standard.object(forKey: "LOGSTRING") as? String {
        //            LOGSTRING = str
        //        } else {
        let file = "file.txt"
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //                //writing
            //                do {
            //                    try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
            //                }
            //                catch {/* error handling here */}
            
            //reading
            do {
                return try String(contentsOf: path, encoding: String.Encoding.utf32)
                //                    print(LOGSTRING)
            }
            catch let error {
                print(error)
                return nil
            }
        } else {
            return nil
        }
        //        }
    }
    var responseCheckout : Bool = false
}


