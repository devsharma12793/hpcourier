//
//  AppDelegate.swift
//  HPCourier
//
//  Created by Devendra Sharma on 15/05/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps
import FacebookCore
import FacebookLogin
import GoogleSignIn



var APPDELEGATE = UIApplication.shared.delegate! as! AppDelegate
let colorOrangeCG :CGColor = UIColor(red: 246.0/255.0, green: 111.0/255.0, blue: 45.0/255.0, alpha: 1).cgColor
let colorOrange  = UIColor(red: 246.0/255.0, green: 111.0/255.0, blue: 45.0/255.0, alpha: 1)
let build = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate,GIDSignInDelegate {
    

    var window: UIWindow?
    var aAnimationView: UIView!
    var result: CGSize?
    var alodingLbl: UILabel!
    var locationManager: CLLocationManager?
    var circularRegion: CLCircularRegion?
    var oldLocation: CLLocation?
    var currentLocation: CLLocation?
    var tabber = MainTabber()

    var monitoringRegions: NSMutableSet?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        aAnimationView = UIView(frame: self.window!.frame)
        result = self.window!.screen.bounds.size
        alodingLbl = UILabel(frame: CGRect(x: CGFloat(((result?.width)! / 2) - 50), y: CGFloat((result?.height)! / 2), width: CGFloat(100), height: CGFloat(40)))
        alodingLbl.textAlignment = .center
        GMSServices.provideAPIKey(kGOOGLE_MAP_API_KEY)
        GMSPlacesClient.provideAPIKey(kGOOGLE_MAP_API_KEY)
        GIDSignIn.sharedInstance().clientID = "903502415631-vjgaco3kd0thing45f4a5hh77cn8pbna.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        self.prepareChargementLoader()
        self.viewNavigation()
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        UITabBar.appearance().selectionIndicatorImage = getImageWithColorPosition(color: UIColor.white, size: CGSize(width:(self.window?.frame.size.width)!/4,height: 49), lineSize: CGSize(width:(self.window?.frame.size.width)!/4, height:2))

        return true
    }
    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
        let rect = CGRect(x:0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x:0, y:size.height-lineSize.height,width: lineSize.width,height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let appId: String = SDKSettings.appId
        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
            return SDKApplicationDelegate.shared.application(app, open: url, options: options)
        }
        return false

        return GIDSignIn.sharedInstance().handle(url as URL?,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        

    }
    var navController: UINavigationController?
    
    func viewNavigation() {
        if LoggedInUser.sharedInstance.token == nil || LoggedInUser.sharedInstance.token == "" {
            self.setLoginRootVC()
        } else {
            LocationClass.sharedInstance.initLocationManager()
            LocationClass.sharedInstance.startMonitoring()
            self.pushToRootVc()
        }
        self.window!.makeKeyAndVisible()
    }

    func setLoginRootVC() {
        var loginViewController =  ViewController()
        let NEWDESIGN_STORYBOARD = UIStoryboard.init(name: "Main", bundle: nil)
        loginViewController = NEWDESIGN_STORYBOARD.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        navController = UINavigationController(rootViewController: loginViewController)
        navController?.isNavigationBarHidden = true
        self.window!.rootViewController = self.navController!
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func pushToRootVc() {
        let NEWDESIGN_STORYBOARD = UIStoryboard.init(name: "Main", bundle: nil)
        APPDELEGATE.tabber = NEWDESIGN_STORYBOARD.instantiateViewController(withIdentifier: "MainTabber") as! MainTabber
        APPDELEGATE.window?.rootViewController = APPDELEGATE.tabber
        APPDELEGATE.tabber.selectedIndex = 0

        }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            let login = Login()
            login.name = fullName as! String
            login.deviceToken = "test tocken"
            login.social_email = email as! String
            login.social_number = userId as! String
            login.social_type = "2"
            APIManager.sharedInstance.LoginUserWithSocial(login: login, completion: { (error:NSError?) in
                APPDELEGATE.removeChargementLoader()
                if error == nil {
                    self.pushToRootVc()
                }
            })
            
            // ...
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    func getDate() -> String {
        
        let format = DateFormatter.init()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return "\(format.string(from: Date.init()))"
        
    }

//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let characterSet = CharacterSet(charactersIn: "<>")
//        let deviceTokenString = deviceToken.description.trimmingCharacters(in: characterSet).replacingOccurrences(of: " ", with: "");
//        print(deviceTokenString)
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")
        AppEventsLogger.activate(application)

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func addChargementLoader() {if let tmpWindow = self.window {tmpWindow.addSubview(self.aAnimationView)}}
    
    func prepareChargementLoader() {
        var progressInd: UIActivityIndicatorView?
        aAnimationView.backgroundColor = UIColor(red: CGFloat(0 / 255.0), green: CGFloat(0 / 255.0), blue: CGFloat(0 / 255.0), alpha: CGFloat(0.50))
        progressInd = UIActivityIndicatorView(activityIndicatorStyle: .white)
        progressInd?.hidesWhenStopped = true
        progressInd!.frame = CGRect(x: CGFloat(((result?.width)! / 2) - 20), y: CGFloat(((result?.height)! / 2) - 20), width: CGFloat(progressInd!.frame.size.width), height: CGFloat(progressInd!.frame.size.height))
        progressInd!.startAnimating()
        progressInd!.tag = 10
        alodingLbl.text = "Loading..."
        alodingLbl.textAlignment = .left
        alodingLbl.textColor = UIColor.white
        alodingLbl.tag = 11
        self.aAnimationView.addSubview(alodingLbl)
        self.aAnimationView.addSubview(progressInd!)
    }
    
    func setChargementFramesForViewMode(_ interfaceOrientation: UIInterfaceOrientation) {
        let progressInd = (aAnimationView.viewWithTag(10)! as! UIActivityIndicatorView)
        if interfaceOrientation == .portrait || (interfaceOrientation == .portraitUpsideDown) {
            progressInd.frame = CGRect(x: CGFloat(40), y: CGFloat(230), width: CGFloat(progressInd.frame.size.width), height: CGFloat(progressInd.frame.size.height))
            alodingLbl.frame = CGRect(x: CGFloat(70), y: CGFloat(280), width: CGFloat(250), height: CGFloat(20))
        }
        else if (interfaceOrientation == .landscapeLeft) || (interfaceOrientation == .landscapeRight) {
            progressInd.frame = CGRect(x: CGFloat(120), y: CGFloat(120), width: CGFloat(progressInd.frame.size.width), height: CGFloat(progressInd.frame.size.height))
            alodingLbl.frame = CGRect(x: CGFloat(150), y: CGFloat(120), width: CGFloat(250), height: CGFloat(20))
        }
    }
    
    func removeChargementLoader() { self.aAnimationView.removeFromSuperview() }
    static var FirebaseToken : String?
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("Firebase registration token: \(fcmToken)")
//        AppDelegate.FirebaseToken = fcmToken
//
//        // TODO: If necessary send token to application server.
//        // Note: This callback is fired at each app startup and whenever a new token is generated.
//    }



}

