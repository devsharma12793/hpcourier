//
//  SocketIOManager.swift
//  HPCourier
//
//  Created by Admin on 27/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    static let shared = SocketIOManager()
    var socket: SocketIOClient!
    
    // defaultNamespaceSocket and swiftSocket both share a single connection to the server
    let manager = SocketManager(socketURL: URL(string: "http://hpcourier.com:9002")!, config: [.log(true), .compress])
    
    override init() {
        super.init()
        
        socket = manager.defaultSocket
    }
    func connectSocket() {
        let token = LoggedInUser.sharedInstance.getAuth()
        
//        self.manager.config = SocketIOClientConfiguration(
//            arrayLiteral: .connectParams(["token": token]), .secure(true)
//        )
        
//        self.manager.config = SocketIOClientConfiguration(arrayLiteral:.connectParams(["token" : token]) )
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
        }
        socket.on("83") { (dataArray, ack) in
            
            print(dataArray.count)
            
        }

        socket.connect()
    }
    
    func receiveMsg(courierBoyID:String) {
        socket.on(courierBoyID) { (dataArray, ack) in
          print(dataArray)
        }
    }

}
