//
//  SLAlert.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation
import UIKit

class SLAlert {

    class func showAlert(str: String)  {
       
        let alert = UIAlertController.init(title: kAPPNAME, message: str, preferredStyle: .alert)
        
        let alertAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        
        alert.addAction(alertAction)

        APPDELEGATE.window?.rootViewController!.present(alert, animated: true, completion: nil)
        
    }
    
    class func showAlert(message: String,completion: @escaping() -> ()){
        let alert = UIAlertController.init(title: kAPPNAME, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction.init(title: "YES", style: .default) { (action) in
            completion()
        }
        let alertActionNO = UIAlertAction.init(title: "NO", style: .default) { (action) in
        }
        alert.addAction(alertAction)
        alert.addAction(alertActionNO)
        APPDELEGATE.window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
    class func showAlertOK(message: String,completion: @escaping() -> ()){
        let alert = UIAlertController.init(title: kAPPNAME, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction.init(title: "OK", style: .default) { (action) in
            completion()
        }
        alert.addAction(alertAction)
        APPDELEGATE.window?.rootViewController!.present(alert, animated: true, completion: nil)
    }

}
