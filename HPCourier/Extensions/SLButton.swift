
//
//  SLTextField.swift
//  Beehive
//
//  Created by Rohan on 17/07/18.
//  Copyright © 2018 SoluLab. All rights reserved.
//

import Foundation
import UIKit

class SLButton: UIButton {
    
    var width: CGFloat = 5
    @IBInspectable
    var padding: CGFloat {
        get {
            return 5
        }
        set {
            width = newValue
        }
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.layer.borderWidth = 1.0
        if(self.isSelected){
            self.layer.borderColor = UIColor(red: 246.0/255.0, green: 111.0/255.0, blue: 45.0/255.0, alpha: 1).cgColor
        }else{
            self.layer.borderColor = UIColor.black.cgColor
        }
        self.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 15)
        self.layer.cornerRadius = 5.0
//        self.textColor = UIColor.white
//        self.font = UIFont(name: "Montserrat-Bold", size: 15)
//        self.layer.cornerRadius = 5.0
//        self.layer.borderColor = UIColor.black.cgColor
//        self.leftViewMode = .always
//        self.leftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: width, height: self.frame.size.height))
    }
    
    
}
