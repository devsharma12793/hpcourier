//
//  GMSAutcompleteCustom.swift
//  Beehive
//
//  Created by Aabid Khan on 7/20/18.
//  Copyright © 2018 SoluLab. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import GoogleMaps

class GMSAutoCompleteTextfield : UITextField,UITableViewDelegate,UITableViewDataSource
{
    var tableViewPlaces: UITableView!
    var tableData=[GMSAutocompletePrediction]()
    var fetcher: GMSAutocompleteFetcher?
    var autoCompleteDelegate : GMSAutocompleteCustomDelegate?
    
    var width: CGFloat = 20
    @IBInspectable
    var padding: CGFloat {
        get {
            return 20
        }
        set {
            width = newValue
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = 20
        self.textColor = UIColor.black
        self.font = UIFont(name: "Montserrat-Regular", size: 16)
        self.leftViewMode = .always
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                        attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])

        self.leftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: width, height: self.frame.size.height))
    }
    override func awakeFromNib()
    {
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        filter.country = "IN"
        tableViewPlaces = UITableView()
        tableViewPlaces.frame = self.frame
        tableViewPlaces.frame.origin.y = self.frame.origin.y + self.frame.height
        tableViewPlaces.frame.size.height = 100
        self.superview!.addSubview(tableViewPlaces)
        
        // Create the fetcher.
        
        fetcher = GMSAutocompleteFetcher(bounds: nil, filter: filter)
        fetcher?.delegate = self
        self.addTarget(self, action: #selector(textFieldDidChange(_:)),for: .editingChanged)
        tableViewPlaces.tableFooterView = UIView()
        tableViewPlaces.delegate = self
        tableViewPlaces.dataSource = self
        self.tableViewPlaces.reloadData()
        self.tableViewPlaces.isHidden = true
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        fetcher?.sourceTextHasChanged(textField.text!)
    }
    
    // MARK: - Tableview delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier:"cell")
        
        cell.selectionStyle =  UITableViewCellSelectionStyle.none
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.textLabel?.textAlignment = NSTextAlignment.left
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.font = UIFont(name: "Montserrat-Regular", size: 16)
        
        cell.textLabel?.text = "\(tableData[indexPath.row].attributedFullText.string)"
        
        return cell
    }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            GMSPlacesClient.shared().lookUpPlaceID("\(tableData[indexPath.row].placeID!)", callback: { (place, error) -> Void in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
    
                guard let place = place else {
                    print("No place details for \(self.tableData[indexPath.row].placeID!)")
                    return
                }
                
                if let dele = self.autoCompleteDelegate
                {
                    self.tableViewPlaces.isHidden = true
                    dele.didSelectAutoSuggestResult(place: place)
                }
                
            })
      }
}

extension GMSAutoCompleteTextfield: GMSAutocompleteFetcherDelegate
{
    func didAutocomplete(with predictions: [GMSAutocompletePrediction])
    {
        tableData.removeAll()
        self.tableViewPlaces.frame.size.width = self.frame.size.width
        
        for prediction in predictions
        {
            tableData.append(prediction)
        }
        if tableData.count > 0
        {
            tableViewPlaces.isHidden = false
        }
        else
        {
            tableViewPlaces.isHidden = true
        }
        tableViewPlaces.reloadData()
    }
    
    func didFailAutocompleteWithError(_ error: Error)
    {
        
    }
}

protocol GMSAutocompleteCustomDelegate
{
    func didSelectAutoSuggestResult(place : GMSPlace)
}
//
//class CustomPlace
//{
//    var latitude : Double = 0
//    var longitude : Double = 0
//    var placeName = ""
//}
