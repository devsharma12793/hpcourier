
//
//  SLTextField.swift
//  Beehive
//
//  Created by Devendra on 17/07/18.
//  Copyright © 2018 Devendra. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
class SLTextField: UITextField {
    
    var width: CGFloat = 5

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        if(boolReg){
            self.layer.borderWidth = 1
            self.font = UIFont(name: "Montserrat-Regular", size: 16)
            self.layer.cornerRadius = 15
            self.clipsToBounds = true
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
            
            self.layer.borderColor = UIColor.black.cgColor

        }else{
            self.layer.borderWidth = 1
            self.font = UIFont(name: "Montserrat-Regular", size: 16)
            self.layer.cornerRadius = 15
            self.clipsToBounds = true
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            
            self.layer.borderColor = self.backgroundColor?.cgColor

        }
    }
 
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var boolReg: Bool = false
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            self.textColor = color
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30 , height: 30))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }

}
