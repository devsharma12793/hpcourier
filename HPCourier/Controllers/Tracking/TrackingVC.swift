//
//  TrackingVC.swift
//  HPCourier
//
//  Created by Devendra on 07/06/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit
import SocketIO
import GoogleMaps

class TrackingVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnHelp: UIButton!
    static let shared = SocketIOManager()
    var socket: SocketIOClient!
    private let locationManager = CLLocationManager()

    @IBOutlet var btnRunningCourier: UIButton!
    @IBOutlet var viewOrders: UIView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var tblOrders: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        btnRunningCourier.layer.cornerRadius = 5
        btnRunningCourier.layer.borderColor = colorOrange.cgColor
        btnRunningCourier.layer.borderWidth = 1
        viewOrders.isHidden = true

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.CourierRunningDetail()

    }
    @IBAction func btnRunningCourierPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if(sender.isSelected){
            viewOrders.isHidden = true
        }else{
            viewOrders.isHidden = false

        }
    }
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let config = URLSessionConfiguration.default      
        let origin = "\(source.latitude),\(source.longitude)"
        let destination = "\(destination.latitude),\(destination.longitude)"

        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGOOGLE_MAP_API_KEY)"

        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    let routes = json["routes"] as! NSArray
                    self.mapView.clear()
                    
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 5
                            
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                            
                            polyline.map = self.mapView
                            
                        }
                    })
                }catch let error as NSError{
                    print("error:\(error)")
                }
            }
        }).resume()

    }
    var userID : String = ""
    var arrayCourier : [[String : AnyObject]] = [[:]]
    var arrayCourierFinal : [[String : AnyObject]] = [[:]]

    func CourierRunningDetail(){
        APPDELEGATE.addChargementLoader()
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                userID = user["id"] as! String
                
            }
        }
        APIManager.sharedInstance.GetRunningCourier(strUser:userID,completion: { (json:[[String:AnyObject]]?,error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            self.arrayCourier.removeAll()
            self.arrayCourierFinal.removeAll()
            if error == nil {
                if(json != nil){
                    self.arrayCourier = json!
                    for i in 0..<self.arrayCourier.count {
                        let data = self.arrayCourier[i]
                        let status = data["status"] as? String
                        if(status == "2"){
                            self.arrayCourierFinal.insert(self.arrayCourier[i], at: self.arrayCourierFinal.count)

                        }else{
                        }
                    }

                    self.tblOrders.dataSource = self
                    self.tblOrders.delegate = self
                    self.tblOrders.reloadData()
                    print(json)
                    
                }else{
                }
            }
        })
        
    }
    func courierBoyLocation(strOrdID :String){
        APPDELEGATE.addChargementLoader()
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                userID = user["id"] as! String
                
            }
        }
        APIManager.sharedInstance.GetCourierBoyLocation(strUser:userID,strorderID:strOrdID ,completion: { (json:[[String:AnyObject]]?,error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            self.arrayCourier.removeAll()
            if error == nil {
                if(json != nil){
                    print(json)
                    let data = json?[0] as? [String:AnyObject]
                    let fromLatitude = data?["from_latitude"] as? String

                    
                    let from_longitude = data?["from_longitude"] as? String

                    let cLatitude = data?["latitude"] as? String

                    let clongitude = data?["longitude"] as? String

                    let courierboy_id = data?["courier_boy_id"] as? String
                    if(courierboy_id != nil){
                        SocketIOManager.shared.connectSocket()
                        SocketIOManager.shared.receiveMsg(courierBoyID: courierboy_id!)
                    }
                    
                    let fromC = CLLocationCoordinate2D(latitude: (cLatitude! as NSString).doubleValue, longitude: (clongitude! as NSString).doubleValue)
                    let ToC = CLLocationCoordinate2D(latitude: (fromLatitude! as NSString).doubleValue, longitude: (from_longitude! as NSString).doubleValue)
                    self.getPolylineRoute(from: fromC, to: ToC)

                }else{
                }
            }
        })
        
    }

    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = mapView // Your map view
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCourierFinal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "custCellTrackingOrders") as! custCellTrackingOrders
        let data = arrayCourierFinal[indexPath.row]
        cell.lblAddFrom.text = data["from_address"] as? String
        cell.lblAddTo.text = data["to_address"] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = arrayCourierFinal[indexPath.row]
        let order_id = data["order_id"] as? String
        if(order_id != nil){
            courierBoyLocation(strOrdID: order_id!)
            btnRunningCourierPressed(btnRunningCourier)
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TrackingVC: CLLocationManagerDelegate {
    // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        // 7
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        
        // 8
        locationManager.stopUpdatingLocation()
    }
}
