//
//  OrderHistoryVC.swift
//  HPCourier
//
//  Created by Admin on 18/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class OrderHistoryVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    var orderDetail : JSONDictionary = [:]
    var arrayCourier : [[String : AnyObject]] = [[:]]
    var sourceCourierData : [String : AnyObject] = [:]
    var destinationCourierData : [String : AnyObject] = [:]
    var courierData : [String : AnyObject] = [:]
    
    var qrCodeData : [String : AnyObject] = [:]
    @IBOutlet var tblViewOrderHistory: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let ordID = orderDetail["order_id"] as? String
        print(orderDetail)
        self.getOrderHistory(strOrdID: ordID!)
      //  getOrderHistory()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        APPDELEGATE.tabber.tabBarController?.tabBar.isHidden = true
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func getOrderHistory(strOrdID:String){
        
        var user_Id : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                user_Id = (user["id"] as? String)!
            }
        }
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.GetOrderHistory(strUser:user_Id,strOrdID: strOrdID,strBranchID: "", completion: { (json:[[String:AnyObject]]?,error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    let dataTracking = json![0]
                    self.arrayCourier = dataTracking["order_tracking"] as! [[String : AnyObject]]
                    self.qrCodeData = dataTracking["order_details"] as! [String : AnyObject]
                    self.courierData = dataTracking["courier_details"] as! [String : AnyObject]
                    self.destinationCourierData = dataTracking["order_destination"] as! [String : AnyObject]
                    self.sourceCourierData = dataTracking["order_source"] as! [String : AnyObject]
                    
                    self.tblViewOrderHistory.dataSource = self
                    self.tblViewOrderHistory.delegate = self
                    self.tblViewOrderHistory.reloadData()

                }else{
                }
            }else{
//                self.tblAddressList.dataSource = self
//                self.tblAddressList.delegate = self
//                self.tblAddressList.reloadData()
                
            }
        })
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCourier.count + 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0){
            let cell = tblViewOrderHistory.dequeueReusableCell(withIdentifier: "cusrqrcell") as! CustOrderQRCell
            cell.viewOrder.dropShadow()
            cell.lblDate.text = qrCodeData["order_date"] as? String
            cell.lblOrderNumber.text = qrCodeData["order_number"] as? String
            let imgUrl = qrCodeData["qr_code_img"] as? String
            
//            if (imgUrl != nil){
//                self.downloadedFrom(link: cell.imgUrl!)
//            }else{
//
//            }

            return cell

        }else if(indexPath.row == 1){
            let cell = tblViewOrderHistory.dequeueReusableCell(withIdentifier: "custorderdestinationcell") as! CustOrderDestinationCell
            cell.viewOrder.dropShadow()
            cell.lblName.text = destinationCourierData["name"] as? String
            cell.lblMobileNumber.text = destinationCourierData["mobile_number"] as? String
            cell.lblMobileNumber.text = destinationCourierData["address"] as? String

            return cell

        }else if(indexPath.row == 2){
            let cell = tblViewOrderHistory.dequeueReusableCell(withIdentifier: "custordersourcecell") as! CustOrderSourceCell
            cell.viewOrder.dropShadow()
            cell.lblName.text = destinationCourierData["user_name"] as? String
            cell.lblMobileNumber.text = destinationCourierData["phone"] as? String
            cell.lblMobileNumber.text = destinationCourierData["address"] as? String

            return cell

        }else if(indexPath.row == 3){
            let cell = tblViewOrderHistory.dequeueReusableCell(withIdentifier: "custordercourierdetailcell") as! CustOrderCourierDetailCell
            cell.viewOrder.dropShadow()
            cell.lblCourierType.text = courierData["courier_type"] as? String
            cell.lblWeight.text = courierData["weight"] as? String
            cell.lblTotalAmount.text = courierData["amount"] as? String
            cell.lblStatus.text = courierData["description"] as? String
            return cell

        }else{
            
            let cell = tblViewOrderHistory.dequeueReusableCell(withIdentifier: "custordertrackingcell") as! CustOrderTrackingCell
            cell.viewOrder.dropShadow()
            let data = arrayCourier[indexPath.row-4]
            cell.lblDate.text = data["tracking_time"] as? String
            cell.lblAddress.text = data["tracking_title"] as? String

            return cell

        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
