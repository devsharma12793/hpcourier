//
//  CustOrderDestinationCell.swift
//  HPCourier
//
//  Created by Admin on 18/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class CustOrderDestinationCell: UITableViewCell {

    @IBOutlet var viewOrder: UIView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblMobileNumber: UILabel!
    @IBOutlet var lblAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
