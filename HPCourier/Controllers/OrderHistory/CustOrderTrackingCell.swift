//
//  CustOrderTrackingCell.swift
//  HPCourier
//
//  Created by Admin on 23/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class CustOrderTrackingCell: UITableViewCell {

    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var viewOrder: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
