//
//  CustOrderQRCell.swift
//  HPCourier
//
//  Created by Admin on 18/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class CustOrderQRCell: UITableViewCell {

    @IBOutlet var viewOrder: UIView!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblOrderNumber: UILabel!
    @IBOutlet var imgQRcode: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
