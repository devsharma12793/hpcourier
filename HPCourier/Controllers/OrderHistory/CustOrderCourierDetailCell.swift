//
//  CustOrderCourierDetailCell.swift
//  HPCourier
//
//  Created by Admin on 22/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class CustOrderCourierDetailCell: UITableViewCell {

    @IBOutlet var viewOrder: UIView!
    @IBOutlet var lblCourierType: UILabel!
    @IBOutlet var lblWeight: UILabel!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var lblStatus: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
