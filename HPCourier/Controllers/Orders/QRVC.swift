//
//  QRVC.swift
//  HPCourier
//
//  Created by Devendra on 06/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class QRVC: UIViewController {
   
    var orderDetail : JSONDictionary = [:]

    @IBOutlet weak var imgQRVC: UIImageView!
    @IBOutlet weak var lblCourierType: UILabel!
    @IBOutlet weak var lblFromAdd: UILabel!
    @IBOutlet weak var lblToAdd: UILabel!
    @IBOutlet weak var lblTotalAmt: UILabel!
    @IBOutlet weak var llblTotalWeight: UILabel!
    @IBOutlet weak var lblCourierBoyName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UpDateViews()
        //qr_code_img
        //courier_type
//        from_address
//        to_address
//        kgs
//        amount
//        courier_boy_name
        // Do any additional setup after loading the view.
    }
    func UpDateViews(){
        lblCourierType.text = orderDetail["courier_type"] as? String
        
        lblFromAdd.text = orderDetail["from_address"] as? String
        
        lblToAdd.text = orderDetail["to_address"] as? String
        lblCourierBoyName.text = orderDetail["courier_boy_name"] as? String
        let totalAMt = orderDetail["amount"] as? String
        let totalWeight = orderDetail["kgs"] as? String
        lblTotalAmt.text = "\(String(describing: totalAMt!))"
        llblTotalWeight.text = "\(String(describing: totalWeight!))"
        let imgUrl = orderDetail["qr_code_img"] as? String
       
        if (imgUrl != nil){
            self.downloadedFrom(link: imgUrl!)
        }else{
        
        }

    }
    
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        
        // contentMode = UIViewContentMode = .scaleAspectFit
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                //self.image = image
                self.imgQRVC.image = image
            }
            }.resume()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
