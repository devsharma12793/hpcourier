//
//  CourierBoyListVC.swift
//  HPCourier
//
//  Created by Devendra on 06/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class CourierBoyListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var branchDetail : AnyObject?
    var UserDetailForOrder : JSONDictionary = [:]
    var orderDetail : JSONDictionary = [:]
    
    var arrayCourier : [[String : AnyObject]] = [[:]]
    @IBOutlet weak var tblViewCourierBoy: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetCourierBoy()
        // Do any additional setup after loading the view.
    }
    func GetCourierBoy(){
       
        let brinch_id  = orderDetail["from_branch_id"]!
        let from_latitude  = orderDetail["from_latitude"]!
        let from_longitude  = orderDetail["from_longitude"]!
        var stringUserID : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                let user_id = user["id"]
                stringUserID  = user_id! as! String
                
            }
        }

        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.GetCourierBoyDetils(brinch_id:brinch_id as! String,userID: stringUserID,fromLat: from_latitude as! String,fromLong:from_longitude as! String, completion: { (json:[[String:AnyObject]]?,error:NSError?) in
            self.arrayCourier.removeAll()
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    self.arrayCourier = json!
                    self.tblViewCourierBoy.dataSource = self
                    self.tblViewCourierBoy.delegate = self
                    self.tblViewCourierBoy.reloadData()
                }else{
                }
            }
        })
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCourier.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! custCellForCourierBoy
        
        cell.viewOrder.dropShadow()
        let data = arrayCourier[indexPath.row]
        cell.lblName.text = data["name"] as? String
        cell.lblMobileNo.text = data["mobile"] as? String

        return cell
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = arrayCourier[indexPath.row]
        let courierboyID = data["id"] as? String

        let finalDict = toJsonDict(courierboyID: courierboyID!)

        submitOrder(dict: finalDict)
    }
    
    
    
    func submitOrder(dict:JSONDictionary){
        
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.AddOrder(dictData: dict, completion: { (json:AnyObject?,error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    
                    let alert = UIAlertController(title: "Hp Courier", message: "Courier Placed Successfully", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HomeMapVC") as! HomeMapVC
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                    }))
                    
                    self.present(alert, animated: true)
                    
                    
                }else{
                    
                }
            }
        })
        
        
    }
    func toJsonDict(courierboyID : String) -> JSONDictionary {
        var dict: JSONDictionary = [:]
        
        dict["user_id"]  = orderDetail["user_id"]! as AnyObject?
        dict[kfrom_address]  = orderDetail[kfrom_address]! as AnyObject?
        dict[kto_address]  = orderDetail[kto_address]! as AnyObject?
        dict["to_user_name"]  = orderDetail["to_user_name"]! as AnyObject?
        dict["to_user_phone"]  = orderDetail["to_user_phone"]! as AnyObject?
        dict["from_user_name"]  = orderDetail["from_user_name"]! as AnyObject?
    dict["from_user_phone"]  = orderDetail["from_user_phone"]! as AnyObject?
        dict["to_branch_id"]  = orderDetail["to_branch_id"]! as AnyObject?
        dict["from_branch_id"]  = orderDetail["from_branch_id"]! as AnyObject?
        dict["kgs"]  = orderDetail["kgs"]! as AnyObject?
        dict["amount"]  = orderDetail["amount"]! as AnyObject?
        dict["courier_type"]  = orderDetail["courier_type"]!  as AnyObject?
        dict["courier_boy_id"]  = courierboyID as AnyObject?
        dict["from_latitude"]  = orderDetail["from_latitude"]! as AnyObject?
        dict["from_longitude"]  = orderDetail["from_longitude"]! as AnyObject?
        dict["to_latitude"]  = orderDetail["to_latitude"]! as AnyObject?
        dict["to_longitude"]  = orderDetail["to_longitude"]! as AnyObject?
        dict["desc"] = "this is Test Data" as AnyObject
        return dict
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
