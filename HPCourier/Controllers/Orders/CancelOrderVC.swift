//
//  CancelOrderVC.swift
//  HPCourier
//
//  Created by Admin on 25/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class CancelOrderVC: UIViewController,UITextViewDelegate {

    
    var orderDetails : [String : AnyObject] = [:]

    @IBOutlet var lblCustName: UILabel!
    @IBOutlet var txtViewReason: UITextView!
    @IBOutlet var btnCancelOrder: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(orderDetails)
        lblCustName.text = orderDetails["courier_boy_name"] as? String
        txtViewReason.layer.cornerRadius = 10
        txtViewReason.layer.borderWidth = 1
        txtViewReason.layer.borderColor = colorOrange.cgColor
        txtViewReason.text = "Please specify the reason"
        txtViewReason.textColor = UIColor.lightGray
        btnCancelOrder.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtViewReason.textColor == UIColor.lightGray {
            txtViewReason.text = nil
            txtViewReason.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if txtViewReason.text.isEmpty {
            txtViewReason.text = "Please specify the reason"
            txtViewReason.textColor = UIColor.lightGray
        }
        txtViewReason.resignFirstResponder()
    }

    @IBAction func btnBackPressed(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelOrderPressed(_ sender: UIButton) {
        if(txtViewReason.text == "" || txtViewReason.textColor == UIColor.lightGray || txtViewReason.text == "Please specify the reason"){
            SLAlert.showAlert(str: "Please specify the reason")
        }else{
            let ordID = orderDetails["order_id"] as? String
            let strReason = txtViewReason.text
            cancelOrder(strOrdID: ordID!,reason:strReason!)
        }
    }
    func cancelOrder(strOrdID:String,reason:String){
        
        var user_Id : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                user_Id = (user["id"] as? String)!
            }
        }
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.CancelOrder(strUser:user_Id,strOrdID: strOrdID,reason: reason, completion: { (error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                
            }else{
                print(error)

            }
        })
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
