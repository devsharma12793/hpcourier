//
//  custCellOrder.swift
//  HPCourier
//
//  Created by Devendra on 08/06/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class custCellOrder: UITableViewCell {

    @IBOutlet weak var viewOrder: UIView!
    @IBOutlet weak var lblCourierBoyName: UILabel!
    @IBOutlet weak var lblFromAdd: UILabel!
    @IBOutlet weak var lblToAdd: UILabel!

    @IBOutlet var btnRepeatOrder: UIButton!
    @IBOutlet var btnCancelOrder: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
