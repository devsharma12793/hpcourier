//
//  custCellForCourierBoy.swift
//  HPCourier
//
//  Created by Devendra on 06/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class custCellForCourierBoy: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var viewOrder: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
