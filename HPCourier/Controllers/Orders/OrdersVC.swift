//
//  OrdersVC.swift
//  HPCourier
//
//  Created by Devendra on 07/06/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class OrdersVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblViewOrders: UITableView!
    @IBOutlet weak var viewRunningCourier: UIView!
    @IBOutlet weak var viewDoneCourier: UIView!
    @IBOutlet weak var btnDoneCourier: UIButton!
    @IBOutlet weak var btnRunnigCourier: UIButton!
    var arrayCourier : [[String : AnyObject]] = [[:]]
    override func viewDidLoad() {
        super.viewDidLoad()
        setUnderView(viewPending: true)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.CourierRunningDetail()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    @IBAction func btnCourierPressed(_ sender: UIButton) {
        if(sender.tag == 0){
            setUnderView(viewPending: true)
            self.CourierRunningDetail()
            
        }else{
            setUnderView(viewPending: false)
            arrayCourier.removeAll()
            self.CourierDoneDetail()
        }
    }
    func setUnderView(viewPending : Bool){
        if(viewPending){
            viewRunningCourier.backgroundColor = UIColor.white
            viewDoneCourier.backgroundColor = colorOrange
        }else{
            viewRunningCourier.backgroundColor = colorOrange
            viewDoneCourier.backgroundColor = UIColor.white

        }
        
    }
    var userID : String = ""
    func CourierRunningDetail(){
        APPDELEGATE.addChargementLoader()
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                userID = user["id"] as! String
                
            }
        }
        APIManager.sharedInstance.GetRunningCourier(strUser:userID,completion: { (json:[[String:AnyObject]]?,error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            self.arrayCourier.removeAll()
            if error == nil {
                if(json != nil){
                    self.arrayCourier = json!
                    self.tblViewOrders.dataSource = self
                    self.tblViewOrders.delegate = self
                    self.tblViewOrders.reloadData()
                    print(json)

                }else{
                }
            }
        })

    }
    func CourierDoneDetail(){
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.GetDoneCourier(strUser:userID, completion: { (json:[[String:AnyObject]]?,error:NSError?) in
            self.arrayCourier.removeAll()
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    self.arrayCourier = json!
                    self.tblViewOrders.dataSource = self
                    self.tblViewOrders.delegate = self
                    self.tblViewOrders.reloadData()
                }else{
                }
            }else{
                self.tblViewOrders.reloadData()

            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCourier.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! custCellOrder

        cell.viewOrder.dropShadow()
        let data = arrayCourier[indexPath.row]
        cell.lblCourierBoyName.text = data["courier_boy_name"] as? String
        cell.lblFromAdd.text = " From : \(data["from_address"] as! String)"
        cell.lblToAdd.text = " To : \(data["to_address"] as! String)"
        cell.btnCancelOrder.layer.cornerRadius = 5
        cell.btnRepeatOrder.layer.cornerRadius = 5
        cell.btnRepeatOrder.tag = indexPath.row
        cell.btnRepeatOrder.addTarget(self, action: #selector(OrdersVC.repetPressed(sender:)), for: .touchUpInside)
        cell.btnCancelOrder.tag = indexPath.row
        cell.btnCancelOrder.addTarget(self, action: #selector(OrdersVC.cancelPressed(sender:)), for: .touchUpInside)
        
        let status = data["status"] as? String
        cell.btnCancelOrder.isHidden = true
        if (status == "1" || status == "2"){
                cell.btnCancelOrder.isHidden = false
        }else{
            cell.btnCancelOrder.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = arrayCourier[indexPath.row]
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "OrderHistoryVC") as! OrderHistoryVC
        nextViewController.orderDetail = data
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @objc func repetPressed(sender: UIButton!) {
        let data = arrayCourier[sender.tag]
        let ordID = data["order_id"] as? String
        if(ordID != nil){
            repeatOrder(strOrdID: ordID!)
        }
    }
    @objc func cancelPressed(sender: UIButton!) {
        let data = arrayCourier[sender.tag]
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CancelOrderVC") as! CancelOrderVC
        nextViewController.orderDetails = data
        self.navigationController?.pushViewController(nextViewController, animated: true)


    }
    var branch_Detail : AnyObject?

    func repeatOrder(strOrdID:String){
        
        var user_Id : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                user_Id = (user["id"] as? String)!
            }
        }
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.RepeatOrder(strUser:user_Id,strOrdID: strOrdID, completion: { (json:[[String:AnyObject]]?,error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    print(json)
                    self.branch_Detail = json! as AnyObject
                    let Data  = json?[0]
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
                    let dictUserFororder = self.toJsonDictorder(result: Data!)
                    nextViewController.branchDetail = self.branch_Detail!
                    nextViewController.UserDetailForOrder = dictUserFororder
                    self.navigationController?.pushViewController(nextViewController, animated: true)

                }else{
                }
            }else{
                
            }
        })
        
    }
    func toJsonDictorder(result:JSONDictionary) -> JSONDictionary {
        var dict: JSONDictionary = [:]
        let fromaddress = result[kfrom_address]
        let toaddress = result[kto_address]
        dict["to_user_name"]  = result["to_user_name"] as AnyObject?
        dict["to_user_phone"] = result["to_user_phone"] as AnyObject?
        dict["from_user_name"]  = result["from_user_name"] as AnyObject?
        dict["from_user_phone"] = result["from_user_phone"] as AnyObject?
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                let user_id = user["id"]
                dict["user_id"]  = user_id! as AnyObject?
                
            }
        }
        
        dict[kfrom_address]  = fromaddress! as AnyObject?
        dict[kto_address] = toaddress as AnyObject?
        dict[kfrom_latitude] = result["from_location_latitude"] as AnyObject?
        dict[kfrom_longitude] = result["from_location_longitude"]  as AnyObject?
        dict[kto_latitude] = result["to_location_latitude"] as AnyObject?
        dict[kto_longitude] = result["to_location_longitude"] as AnyObject?
        
        return dict
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        layer.shadowRadius = 1
        
//        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
