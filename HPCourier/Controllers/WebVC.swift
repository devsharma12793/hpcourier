//
//  WebVC.swift
//  HPCourier
//
//  Created by Admin on 26/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class WebVC: UIViewController,UIWebViewDelegate {

    @IBOutlet var viewDrow: UIView!
    @IBOutlet var webView: UIWebView!
    
    @IBOutlet var viewTitle: UILabel!
    var strViewTitle : String = ""
    var strWebURL : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTitle.text = strViewTitle
        let url = URL (string: strWebURL)
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
        // Do any additional setup after loading the view.

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
