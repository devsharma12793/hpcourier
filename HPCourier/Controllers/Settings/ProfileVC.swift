//
//  ProfileVC.swift
//  HPCourier
//
//  Created by Admin on 26/10/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController,UITextViewDelegate,UITextFieldDelegate {

    @IBOutlet var txtName: SLTextField!
    @IBOutlet var txtMobileNumber: SLTextField!
    @IBOutlet var txtViewAddress: UITextView!
    @IBOutlet var btnSave: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtViewAddress.layer.cornerRadius = 10
        txtViewAddress.layer.borderWidth = 1
        txtViewAddress.layer.borderColor = UIColor.black.cgColor
        txtViewAddress.text = "Please Enter Your Address"
        txtViewAddress.textColor = UIColor.lightGray
        GetCustomerDetail()
        btnSave.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    func GetCustomerDetail(){
        
        var user_Id : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                user_Id = (user["id"] as? String)!
            }
        }
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.GetCustomerDetail(strUser:user_Id, completion: { (json:JSONDictionary?,error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    print(json)
                    self.txtName.text = json?["name"] as? String
                    self.txtMobileNumber.text = json?["phone"] as? String
                    self.txtViewAddress.text = json?["address"] as? String
                    if self.txtViewAddress.text.isEmpty {
                        self.txtViewAddress.text = "Please Enter Your Address"
                        self.txtViewAddress.textColor = UIColor.lightGray
                    }else{
                        self.txtViewAddress.textColor = UIColor.black
                    }
                }else{
                }
            }else{
                
            }
        })
        
    }

    
    func saveCustomerDetail(){
        
        var user_Id : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                user_Id = (user["id"] as? String)!
            }
        }
        var address:String = ""
        if(txtViewAddress.text != "" && txtViewAddress.text != "Please Enter Your Address"){
            address = txtViewAddress.text
        }else{
            address = ""
        }
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.SaveCustomerDetail(strUser:user_Id,name: txtName.text!,mobileNo: txtMobileNumber.text!,address: address, completion: { (json:JSONDictionary?,error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    print(json)
                    SLAlert.showAlertOK(message: "User Detail Updated Successfully", completion: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }else{
                }
            }else{
                
            }
        })
        
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtViewAddress.textColor == UIColor.lightGray {
            txtViewAddress.text = nil
            txtViewAddress.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if txtViewAddress.text.isEmpty {
            txtViewAddress.text = "Please Enter Your Address"
            txtViewAddress.textColor = UIColor.lightGray
        }
        txtViewAddress.resignFirstResponder()
    }

    @IBAction func btnSavePressed(_ sender: UIButton) {
        saveCustomerDetail()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
