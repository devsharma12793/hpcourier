//
//  SettingVC.swift
//  HPCourier
//
//  Created by Devendra on 07/06/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnRateUsPressed(_ sender: UIButton) {
    }
    @IBAction func btnAboutUsPressed(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        nextViewController.strWebURL = "http://hpcourier.com/about.html"
        nextViewController.strViewTitle = "ABOUT US"

        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
//    
    @IBAction func btnHelpPressed(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        nextViewController.strWebURL = "http://hpcourier.com/help.html"
        nextViewController.strViewTitle = "HELP"
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    @IBAction func btnEditProfilePressed(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    @IBAction func btnLogoutPressed(_ sender: UIButton) {
       
        let alert = UIAlertController(title: "Hp Courier", message: "Do you want to logout from application ? ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            LoggedInUser.sharedInstance.token = ""
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)

            APPDELEGATE.setLoginRootVC()
            
        }))
        
        self.present(alert, animated: true)


    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
