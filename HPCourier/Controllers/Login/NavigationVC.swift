//
//  NavigationVC.swift
//  HPCourier
//
//  Created by Devendra on 04/06/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class NavigationVC: UIViewController {

    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTO: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        txtFrom.layer.borderWidth = 2
        txtFrom.layer.cornerRadius = 10
        txtTO.layer.cornerRadius = 10
        txtTO.layer.borderWidth = 2
        txtFrom.layer.borderColor = UIColor(red: 246.0/255.0, green: 111.0/255.0, blue: 45.0/255.0, alpha: 1).cgColor
        txtTO.layer.borderColor =  UIColor.white.cgColor //UIColor(red: 246.0/255.0, green: 111.0/255.0, blue: 45.0/255.0, alpha: 1).cgColor        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
