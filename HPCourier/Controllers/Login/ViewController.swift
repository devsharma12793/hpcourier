//
//  ViewController.swift
//  HPCourier
//
//  Created by Devendra Sharma on 15/05/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookLogin
import FacebookCore
import FacebookShare
import FBSDKLoginKit
class ViewController: UIViewController,UITextFieldDelegate,GIDSignInUIDelegate {

    @IBOutlet weak var btnSignIN: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet var viewForgotPassword: UIView!
    @IBOutlet weak var btnSendEmail: UIButton!
    @IBOutlet weak var txtForGotPass: UITextField!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var signInButton: GIDSignInButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self

        txtEmail.text = "Deven@gmail.com"
        txtPassword.text = "123456"
        LocationClass.sharedInstance.initLocationManager()
        LocationClass.sharedInstance.startMonitoring()
        btnFacebook.addTarget(self, action: #selector(self.loginButtonClicked), for: .touchUpInside)
        
        viewForgotPassword.center = self.view.center;
        viewForgotPassword.layer.cornerRadius = 5
        self.view.addSubview(viewForgotPassword)
        self.viewForgotPassword.isHidden = true

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func fetchUserProfile()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture.width(480).height(480)"])
        
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                print("Error took place: \(error)")
            }
            else
            {
                let data:[String:AnyObject] = result as! [String : AnyObject]
                let userName : NSString? = data["name"]! as? NSString
                let facebookID : NSString? = data["id"]! as? NSString
                let email : NSString? = data["email"]! as? NSString
                let login = Login()
                login.name = userName as! String
                login.deviceToken = "test tocken"
                login.social_email = email! as String
                login.social_number = facebookID as! String
                login.social_type = "1"
                APIManager.sharedInstance.LoginUserWithSocial(login: login, completion: { (error:NSError?) in
                    APPDELEGATE.removeChargementLoader()
                    if error == nil {
                        let NEWDESIGN_STORYBOARD = UIStoryboard.init(name: "Main", bundle: nil)
                        APPDELEGATE.tabber = NEWDESIGN_STORYBOARD.instantiateViewController(withIdentifier: "MainTabber") as! MainTabber
                        APPDELEGATE.window?.rootViewController = APPDELEGATE.tabber
                        APPDELEGATE.tabber.selectedIndex = 0
                    }
                })

                
            }
        })
    }

    func getNewPassword() {
        APIManager.sharedInstance.forgotPasswordUser(email: txtForGotPass.text!) { (error:NSError?) in
            
            APPDELEGATE.removeChargementLoader()
            self.txtForGotPass.text = ""
            self.viewForgotPassword.isHidden = true
        }
    }

    override func viewWillAppear(_ animated: Bool) {
      self.navigationController?.setNavigationBarHidden(true, animated: true)
     
    }
    @IBAction func didTapSignOut(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
    }
    @IBAction func didTapSignIn(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().signIn()
    }

    @IBAction func btnClosePressed(_ sender: UIButton) {
        viewForgotPassword.isHidden = true
    }
    @IBAction func btnForgotPassPressed(_ sender: UIButton) {
        viewForgotPassword.isHidden = false
    }
    func textFieldAlert() -> Bool {
        if (txtForGotPass.text == "") {
            alertVIewshow(sender: "Email Field Can't be Empty")
            return false
        }
        else if txtForGotPass.text!.validateEmail() == false {
            alertVIewshow(sender: "Please Enter Valid Email Address")
            return false
        }
        return true
    }
    func alertVIewshow(sender : String){
        let alert = UIAlertController(title: "Hp Courier", message: sender, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func btnSendEmailPressed(_ sender: UIButton) {
        txtForGotPass.resignFirstResponder()
        if self.textFieldAlert() {
            APPDELEGATE.addChargementLoader()
            self.getNewPassword()
        }

        
    }
    @IBAction func btnSIgnINnPressed(_ sender: UIButton) {
       
        if self.validateAllData() == true {
            let login = Login()
            login.deviceToken = "testTocken" //AppDelegate.FirebaseToken
            login.email = txtEmail.text!
            login.password = txtPassword.text!
            login.login_type = "normal"
            APPDELEGATE.addChargementLoader()
            APIManager.sharedInstance.LoginUser(login: login, completion: { (error:NSError?) in
                APPDELEGATE.removeChargementLoader()
                if error == nil {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MainTabber") as! MainTabber
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            })
        }
    }
    func validateAllData() -> Bool {
        let strEmail = txtEmail.text
        let strPassword = txtPassword.text
        if strEmail?.count == 0 || strEmail == "" {
            SLAlert.showAlert(str: "PLease Enter Email")
            return false
        }
        else if strPassword?.count == 0 || strPassword == "" {
          //  let alert = UIAlertView(title: " Alert", message: "All field are mandatory", delegate: self, cancelButtonTitle: "Ok")
           // alert.show()
            SLAlert.showAlert(str: "PLease Enter Password")
            return false
        }
        else {
            txtEmail.resignFirstResponder()
            txtPassword.resignFirstResponder()
            //self.getLoginWebService()
        }
        
        return true
    }
    func returnFBUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, first_name, last_name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                print("Error: \(error)")
            }
            else
            {
//                let uniqueId = result.valueForKey("id") as! NSString!
//                let firstName = result.valueForKey("first_name") as! NSString!
//                let lastName = result.valueForKey("last_name") as! NSString!
//                print(uniqueId) // This works
//                print(firstName)
//                print(lastName)
            }
        })
    }

    @objc func loginButtonClicked() {
        let loginManager=LoginManager()
        loginManager.logIn(readPermissions: [ReadPermission.publicProfile,ReadPermission.custom("email")], viewController : self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in")
                print(accessToken)
                let login = Login()
                if let _ = FBSDKAccessToken.current()
                {
                    self.fetchUserProfile()
                }

                // this is an instance function

//                login.name = fullName as! String
//                login.deviceToken = "test tocken"
//                login.social_email = email as! String
//                login.social_number = userId as! String
//                login.social_type = "2"
//                APIManager.sharedInstance.LoginUserWithSocial(login: login, completion: { (error:NSError?) in
//                    APPDELEGATE.removeChargementLoader()
//                    if error == nil {
//                        self.pushToRootVc()
//                    }
//                })
            }
        }
    }



    
    @IBAction func btnCreateACPressed(_ sender: UIButton) {
       
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "RegVC") as! RegVC
        //self.presentViewController(nextViewController, animated:true, completion:nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

