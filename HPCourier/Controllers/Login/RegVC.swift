//
//  RegVC.swift
//  HPCourier
//
//  Created by Devendra Sharma on 17/05/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class RegVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtCpass: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSignIN: UIButton!
    @IBAction func btnSignINnPressed(_ sender: UIButton) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    // MARK: - Button CLick Event

    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignInnPressed(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    
    }
    @IBAction func btnSIgnUpPressed(_ sender: Any) {
        self.validateAllDataForSignUp()
    }
    
    // MARK: - TextFIeld Delegate
    var charSet: CharacterSet!

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtName {
            
            charSet = CharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz' ").inverted
            
            let strArr = string.components(separatedBy: charSet)
            let filteredString = strArr.joined(separator: "")
            return (string == filteredString)
        }else if textField == txtMobileNo
        {
            charSet = CharacterSet.init(charactersIn: "1234567890").inverted
            let strArr = string.components(separatedBy: charSet)
            let filteredString = strArr.joined(separator: "")
            return (string == filteredString)
            
        }
        return true
    }
    
    func passwordIsValid(_ password: String) -> Bool {
        if password.characters.count < 6 { return false }
        let specialCharacters = "@!#€%&/()[]=?$§*'"
        if password.components(separatedBy: CharacterSet.init(charactersIn: "\(specialCharacters)")).count < 2 { return true }
        if password.components(separatedBy: CharacterSet.init(charactersIn: "0123456789")).count < 2 { return true }
        return false
        
    }

    
    
    // MARK: - Field Validation
    
    func validateAllDataForSignUp() {
        
//        if txtName.text?.characters.count == 0 || txtMobileNo.text?.characters.count == 0 || txtEmail.text?.characters.count == 0 || txtPass.text?.characters.count == 0 || txtCpass.text?.characters.count == 0 {
//            let myAlert = UIAlertView(title: "Beehive", message: "Please fill All Details", delegate: self, cancelButtonTitle: "OK")
//            myAlert.show()
//            return
//        }
//        if (txtPass.text?.characters.count)! < 6 || (txtCpass.text?.characters.count)! < 6 {
//            let myAlert = UIAlertView(title: "Beehive", message: "Please make password with minimum 8 alphanumeric characters", delegate: self, cancelButtonTitle: "OK")
//            myAlert.show()
//            return
//        }
        self.getSignUpWebService()
    }

    
    // MARK: - API Calling

    func getSignUpWebService() {
        
        let user = Register()
        
        var localTimeZoneName: String { return TimeZone.current.identifier }
        user.name = txtName.text!
        user.email = txtEmail.text!
        user.phone = txtMobileNo.text! // firstText is UITextField
        user.password = txtPass.text!
        user.c_password = txtCpass.text!
        user.device_token = "testDevice"
       // user.postalCode = txtPostalCode.text
       // user.terms = "Y"
       // user.user_timezone = localTimeZoneName
//        if(iscrew){
//            user.role = 4
//        }else{
//            user.role = 3
//        }
//        user.devicetoken = AppDelegate.FirebaseToken
//        
        APIManager.sharedInstance.RegisterNewUser(register: user) { (error:NSError?) in
            if error == nil {_=self.navigationController?.popViewController(animated: true)} else {}
            
           // let leftMenuvC = NEWDESIGN_STORYBOARD.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            //self.navigationController?.pushViewController(leftMenuvC, animated: true)
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
