//
//  ListOfAddressVC.swift
//  HPCourier
//
//  Created by Devendra on 14/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

protocol selectAddressDelegate {
    func setAddress(address: AddressNew, boolFrom : Bool)
}

class ListOfAddressVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblAddressList: UITableView!
    var arrayAddresses : [[String : AnyObject]] = [[:]]
    var checkBoolFrom : Bool = false
    var delegate: selectAddressDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAddressList()
        
    }
    @IBAction func btnAddAddressPressed(_ sender: UIButton) {
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func getAddressList(){
       
        var user_Id : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                user_Id = (user["id"] as? String)!
                
            }
        }
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.GetAddress(strUser:user_Id, completion: { (json:[[String:AnyObject]]?,error:NSError?) in
            self.arrayAddresses.removeAll()
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    self.arrayAddresses = json!
                    self.tblAddressList.dataSource = self
                    self.tblAddressList.delegate = self
                    self.tblAddressList.reloadData()
                }else{
                }
            }else{
                self.tblAddressList.dataSource = self
                self.tblAddressList.delegate = self
                self.tblAddressList.reloadData()

            }
        })
        
    }
    func deleteAddressList(addId:String){
        
        var user_Id : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                user_Id = (user["id"] as? String)!
                
            }
        }
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.DeleteAddress(strUser:user_Id,strAddId:addId , completion: { (error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                    self.getAddressList()
            }
            })
        
    }

    // MARK: - tableVIewDatasource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(arrayAddresses.count == 0 || arrayAddresses == nil){
            return 0

        }else{
            return arrayAddresses.count

        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! custAddressCell
        
        cell.viewOrder.dropShadow()
        let data = arrayAddresses[indexPath.row]
        cell.lblName.text = data["name"] as? String
        cell.lblMobile.text = data["number"] as? String
        cell.lblAddress.text = data["address"] as? String
        cell.btnDeleteAddress.tag = indexPath.row
        cell.btnDeleteAddress.addTarget(self, action: #selector(ListOfAddressVC.pressed(sender:)), for: .touchUpInside)

        return cell
    }
    
    
    
    @objc func pressed(sender: UIButton!) {
        let data = arrayAddresses[sender.tag]
        let addId = data["id"] as? String
        if(addId != nil){
            self.deleteAddressList(addId: addId!)
        }

    }

    // MARK: - tableVIewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = arrayAddresses[indexPath.row]
        let straddress = data["address"]
        let address = AddressNew()
        address.populateWithJson(dict: data)
        print(address)
     //   address.name = 
        self.delegate.setAddress(address: address, boolFrom: checkBoolFrom)
        self.navigationController?.popViewController(animated: true)

//        if(checkBoolFrom){
//            HomeMapVC.sharedInstance.strFromAdd = straddress
//
//        }else{
//            HomeMapVC.sharedInstance.strToAdd = straddress
//        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
