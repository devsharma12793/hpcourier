//
//  HomeMapVC.swift
//  HPCourier
//
//  Created by Devendra on 07/06/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

class HomeMapVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,GMSAutocompleteCustomDelegate,GMSMapViewDelegate,CLLocationManagerDelegate,selectAddressDelegate {

    @IBOutlet weak var txtFullAddress: GMSAutoCompleteTextfield!
    var sliderValue:Int = 100
    var radiusMapView: GMSMapView!
    var radiusCircle: GMSCircle!
    var radiusValue = 0
    var actualLocation: CLLocation!
    var locationLatitude = CLLocationDegrees()
    var locationLongitude = CLLocationDegrees()
    var addressString = ""
    var circleCenter = CLLocationCoordinate2D()
    var currentLocation: CLLocation?
    var contact = contacts
    
    @IBOutlet weak var tblViewlistAddress: UITableView!
    
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    
    @IBOutlet var viewAddress: UIView!
    @IBOutlet weak var btnSaveAddress: UIButton!
    @IBOutlet weak var txtname: SLTextField!
    @IBOutlet weak var txtmobile: SLTextField!
    @IBOutlet weak var txtStreetNo: SLTextField!
    @IBOutlet weak var txtStreetName: SLTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtPostalCode: SLTextField!
    @IBOutlet weak var txtCity: SLTextField!
    @IBOutlet weak var txtState: SLTextField!
    @IBOutlet weak var txtCountry: SLTextField!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnSelectAddessList: UIButton!
    @IBOutlet weak var btnFrom: UIButton!
    @IBOutlet weak var btnTo: UIButton!
  
    var strFromAdd : String?
    var strToAdd : String?

    var Fromlatitude : String?
    var Fromlongitude : String?

    var Tolatitude : String?
    var Tolongitude : String?

    var strCheckFrom : String?

    var callMapView : Bool = true
    
    public class var sharedInstance: HomeMapVC {
        struct Singleton {
            static let instance: HomeMapVC = HomeMapVC()
        }
        return Singleton.instance
    }
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
      
        viewAddress.frame = CGRect(x: 10, y: 50, width: self.view.bounds.width - 20 , height: self.view.bounds.height - 100)
        viewAddress.layer.borderColor = UIColor.white.cgColor
        viewAddress.layer.borderWidth = 2
        viewAddress.layer.cornerRadius = 5
        self.view.addSubview(viewAddress)
        txtFullAddress.autoCompleteDelegate = self
        viewAddress.isHidden = true
        self.setButtonForView(sender: btnFrom)
        self.setButtonForView(sender: btnTo)
        self.setButtonForView(sender: btnSaveAddress)
        self.setButtonForView(sender: btnSelectAddessList)
        tblViewlistAddress.isHidden = true
        btnFrom.layer.borderColor = colorOrangeCG
        btnTo.layer.borderColor = colorOrangeCG
        btnSubmit.layer.cornerRadius = 5
        APPDELEGATE.addChargementLoader()
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { timer in
                self.mapViewCustomization()
            }
        }

         if let userData = UserDefaults.standard.object(forKey: kAddressDataLocal) as? Data {
          
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? [JSONDictionary] {
                arrayAddress = user
            }
        }
    }
    func setAddress(address: AddressNew,boolFrom : Bool) {
        if (boolFrom){
            let add = address
            from_user_nameS = add.name!
            from_user_phoneS = add.number!
            Fromlatitude = add.latitude
            Fromlongitude = add.longitude
            btnFrom.setTitle(add.address, for: .normal)
            strCheckFrom = "from"
        }else{
            let add = address
            to_user_nameS = add.name!
            to_user_phoneS = add.number!
            Tolatitude = add.latitude
            Tolongitude = add.longitude
            btnTo.setTitle(add.address, for: .normal)
            strCheckFrom = "to"

        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
//        if(callMapView){
//            callMapView = false
//        }
        
        if(strFromAdd != nil && strFromAdd != ""){
            btnFrom.setTitle(strFromAdd, for: .normal)
        }
        if(strToAdd  != nil && strToAdd != ""){
            btnTo.setTitle(strToAdd, for: .normal)
        }
        self.mapViewCustomization()

    }
    var branch_Detail : AnyObject?
    func validateAllDatas() -> Bool {
        let fromaddress = btnFrom.titleLabel?.text
        let toaddress = btnTo.titleLabel?.text
        if fromaddress?.count == 0 || fromaddress == "" {
            SLAlert.showAlert(str: "PLease Enter From Address")
            return false
        }
        else if toaddress?.count == 0 || toaddress == "" {
            SLAlert.showAlert(str: "PLease Enter To Address")
            return false
        }
        else if Fromlatitude?.count == 0 || Fromlatitude == "" {
            SLAlert.showAlert(str: "From Address Invalid")
            return false
        }
        else if Fromlongitude?.count == 0 || Fromlongitude == "" {
            SLAlert.showAlert(str: "From Address Invalid")
            return false
        }
        else if Tolatitude?.count == 0 || Tolatitude == "" {
            SLAlert.showAlert(str: "To Address Invalid")
            return false
        }
        else if Tolongitude?.count == 0 || Tolongitude == "" {
            SLAlert.showAlert(str: "To Address Invalid")
            return false
        }

        else {
            //self.getLoginWebService()
        }
        
        return true
    }

    @IBAction func btnProceedCourierPressed(_ sender: UIButton) {
      
        if self.validateAllDatas() == true {
            let dict = self.toJsonDict()
            APPDELEGATE.addChargementLoader()
            APIManager.sharedInstance.GetBranch(dictData: dict, completion: { (json:AnyObject?,error:NSError?) in
                APPDELEGATE.removeChargementLoader()
                if error == nil {
                    if(json != nil){
                        self.branch_Detail = json!
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
                        let dictUserFororder = self.toJsonDictorder()
                        nextViewController.branchDetail = self.branch_Detail!
                        nextViewController.UserDetailForOrder = dictUserFororder
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                    }else{
                        
                    }
                }
            })
        }
        
    }

        //    "from_address":"University Road , Rajkot"
        //    "from_latitude":"72.535598"
        //    "from_longitude":"23.0747676"
        //    "to_address":"Manek Chock, Admedabad"
        //    "to_latitude":"72.5995843"
        //    "to_longitude":"22.9961698"
        
    
    func toJsonDict() -> JSONDictionary {
        var dict: JSONDictionary = [:]
        let fromaddress = btnFrom.titleLabel?.text
        let toaddress = btnTo.titleLabel?.text
        if(fromaddress != nil && fromaddress != ""){
            dict[kfrom_address]  = fromaddress! as AnyObject?
        }
        if(toaddress != nil && toaddress != ""){
           dict[kto_address] = toaddress as AnyObject?
        }
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                let user_id = user["id"]
                dict["user_id"]  = user_id! as AnyObject?
                
            }
        }
        dict[kfrom_latitude] = Fromlatitude as AnyObject?
        dict[kfrom_longitude] = Fromlongitude as AnyObject?
        dict[kto_latitude] = Tolatitude as AnyObject?
        dict[kto_longitude] = Tolongitude as AnyObject?

        return dict
    }
    func toJsonDictorder() -> JSONDictionary {
        var dict: JSONDictionary = [:]
        let fromaddress = btnFrom.titleLabel?.text
        let toaddress = btnTo.titleLabel?.text
        dict["to_user_name"]  = to_user_nameS as AnyObject?
        dict["to_user_phone"] = to_user_phoneS as AnyObject?
        dict["from_user_name"]  = from_user_nameS as AnyObject?
        dict["from_user_phone"] = from_user_phoneS as AnyObject?
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                let user_id = user["id"]
                dict["user_id"]  = user_id! as AnyObject?
                
            }
        }

        if(fromaddress != nil && fromaddress != ""){
            dict[kfrom_address]  = fromaddress! as AnyObject?
        }
        if(toaddress != nil && toaddress != ""){
            dict[kto_address] = toaddress as AnyObject?
        }
        dict[kfrom_latitude] = Fromlatitude as AnyObject?
        dict[kfrom_longitude] = Fromlongitude as AnyObject?
        dict[kto_latitude] = Tolatitude as AnyObject?
        dict[kto_longitude] = Tolongitude as AnyObject?
        
        return dict
    }

    @IBAction func btnCloseAddressViewPressed(_ sender: UIButton) {
        if(viewAddress.isHidden == false){
        self.setView(view: viewAddress, hidden: true)
        }
    }
    @IBAction func btnSelectAddressPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if(sender.isSelected == true){
            tblViewlistAddress.isHidden = false
            tblHeight.constant = tblViewlistAddress.contentSize.height

        }else{
            tblViewlistAddress.isHidden = true
          

        }
    }
    var contactData = AddressDict()
    var arrayAddress = [JSONDictionary]()
    var boolcheckaddin : Bool = false
    var to_user_nameS : String = ""
    var to_user_phoneS : String = ""
    var from_user_nameS : String = ""
    var from_user_phoneS : String = ""

    @IBAction func btnSaveAddressPressed(_ sender: UIButton) {
        
        if self.validateAllData() == true {
            
            //AddressDict.toJsonDict(<#T##AddressDict#>)
            let name = txtname.text
            let mobile = txtmobile.text
            let strAddress = txtFullAddress.text
            let strstreetNo = txtStreetNo.text
            let strstreetName = txtStreetName.text
            let strPincode = txtPostalCode.text
            let strCity = txtCity.text
            let strState = txtState.text
            let strcountry = txtCountry.text
            var lat = ""
            var long = ""
            if(strCheckFrom == "from"){
                lat = Fromlatitude!
                long = Fromlongitude!
                from_user_nameS = name!
                from_user_phoneS = mobile!
            }else{
                 lat = Tolatitude!
                 long = Tolongitude!
                to_user_nameS = name!
                to_user_phoneS = mobile!
            }
            for checkName in arrayAddress{
                let CustName = checkName["name"] as! String
                if name == CustName{
                    boolcheckaddin = true
                    break
                }else{
                    boolcheckaddin = false

                }

            }
            if(!boolcheckaddin){
                let data = Address.init(name : name ,mobile : mobile, addressString: strAddress, streetNo: strstreetNo, Street: strstreetName, pincode: strPincode, latitute : lat, longitude:long, city: strCity, state: strState, country: strcountry)
                contactData.populateWithJson(dict: data)
                let dataDict  = AddressDict.toJsonDict(contactData)
                print(dataDict)
                arrayAddress.insert(dataDict(), at: arrayAddress.count)
                print(arrayAddress)
                if(strCheckFrom == "from"){
                    btnFrom.setTitle(strAddress, for: .normal)
                }else{
                    btnTo.setTitle(strAddress, for: .normal)

                }
                self.saveUserData()
                if(viewAddress.isHidden == false){
                self.setView(view: viewAddress, hidden: true)
                }
            }else{
                
                if(strCheckFrom == "from"){
                    btnFrom.setTitle(strAddress, for: .normal)
                }else{
                    btnTo.setTitle(strAddress, for: .normal)
                    
                }
                if(viewAddress.isHidden == false){
                    self.setView(view: viewAddress, hidden: true)
                }

            }
            //Address.init(name : name,mobile : mobile,addressString: strAddress, streetNo: strstreetNo, Street: strstreetName, pincode: strPincode, city: strCity, state: strState, country: strcountry)

        }
    }
    func saveUserData() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: kAddressDataLocal)
        userDefaults.synchronize()
        let userData = NSKeyedArchiver.archivedData(withRootObject: arrayAddress)
        userDefaults.set(userData, forKey: kAddressDataLocal)
        userDefaults.synchronize()
    }

    func validateAllData() -> Bool {
        let straddress = txtFullAddress.text
        let strstreetNo = txtStreetNo.text
        let strstreetName = txtStreetName.text
        let strpostalcode = txtPostalCode.text
        let strcity = txtCity.text
        let strstate = txtState.text
        let strcountry = txtCountry.text
        
        if straddress?.count == 0 || straddress == "" {
            SLAlert.showAlert(str: "PLease Enter Address")
            return false
        }
        else if strstreetNo?.count == 0 || strstreetNo == "" {
            SLAlert.showAlert(str: "PLease Enter Street/Flat No")
            return false
        }
        else if strstreetName?.count == 0 || strstreetName == "" {
            SLAlert.showAlert(str: "PLease Enter StreetName")
            return false
        }
        else if strpostalcode?.count == 0 || strpostalcode == "" {
            SLAlert.showAlert(str: "PLease Enter PostalCode")
            return false
        }
        else if strcity?.count == 0 || strcity == "" {
            SLAlert.showAlert(str: "PLease Enter City Name")
            return false
        }
        else if strstate?.count == 0 || strstate == "" {
            SLAlert.showAlert(str: "PLease Enter State Name")
            return false
        }
        else if strcountry?.count == 0 || strcountry == "" {
            SLAlert.showAlert(str: "PLease Enter Country Name")
            return false
        }
        else {
            
            if(strCheckFrom == "from"){
                if Fromlatitude?.count == 0 || Fromlatitude == "" || Fromlatitude == nil {
                    SLAlert.showAlert(str: "Location Not Found")
                    return false
                }
                else if Fromlongitude?.count == 0 || Fromlongitude == "" || Fromlongitude == nil {
                    SLAlert.showAlert(str: "Location Not Found")
                    return false
                }
            }else{
                 if Tolongitude?.count == 0 || Tolongitude == "" || Tolatitude == nil {
                    SLAlert.showAlert(str: "Location Not Found")
                    return false
                }
                 if Tolatitude?.count == 0 || Tolatitude == "" || Tolatitude == nil {
                    SLAlert.showAlert(str: "Location Not Found")
                    return false
                }
            }
            txtFullAddress.resignFirstResponder()
            txtStreetNo.resignFirstResponder()
            txtStreetName.resignFirstResponder()
            txtPostalCode.resignFirstResponder()
            txtCity.resignFirstResponder()
            txtState.resignFirstResponder()
            txtCountry.resignFirstResponder()
            //self.getLoginWebService()
        }
        
        return true
    }

    
    func mapViewCustomization() {
        
        if(Fromlatitude == "" || Fromlongitude == "" || Tolatitude == "" || Tolongitude == "" || Fromlatitude == nil || Fromlongitude == nil || Tolatitude == nil || Tolongitude == nil ){
            if(LocationClass.sharedInstance.currentLocation != nil){
                circleCenter = (LocationClass.sharedInstance.currentLocation?.coordinate)!
                mapView.frame = self.view.bounds
                let camera = GMSCameraPosition.camera(withLatitude: circleCenter.latitude, longitude: circleCenter.longitude, zoom: 15.0)
                radiusMapView = GMSMapView.map(withFrame: CGRect.init(x: 0, y: 0, width: mapView.frame.size.width, height: mapView.frame.size.height), camera: camera)
                radiusMapView.mapType = .normal
                radiusMapView.delegate = self
                radiusMapView.isMyLocationEnabled = true
                self.mapView.addSubview(radiusMapView)
                self.perform(#selector(self.drowCircle), with: nil, afterDelay: 1.0)
                timer.invalidate()
            }else{
                
                APPDELEGATE.removeChargementLoader()

            }
        }else{
            
            if(strCheckFrom == "from"){
                circleCenter = CLLocationCoordinate2D.init(latitude: CLLocationDegrees.init(Fromlatitude!)!, longitude: CLLocationDegrees.init(Fromlongitude!)!)
                let camera = GMSCameraPosition.camera(withLatitude: circleCenter.latitude, longitude: circleCenter.longitude, zoom: 15.0)
                radiusMapView = GMSMapView.map(withFrame: CGRect.init(x: 0, y: 0, width: mapView.frame.size.width, height: mapView.frame.size.height), camera: camera)
                radiusMapView.mapType = .normal
                radiusMapView.delegate = self
                radiusMapView.isMyLocationEnabled = true
                self.mapView.addSubview(radiusMapView)
                self.perform(#selector(self.drowCircle), with: nil, afterDelay: 1.0)

            }else{
                circleCenter = CLLocationCoordinate2D.init(latitude: CLLocationDegrees.init(Tolatitude!)!, longitude: CLLocationDegrees.init(Tolongitude!)!)
                let camera = GMSCameraPosition.camera(withLatitude: circleCenter.latitude, longitude: circleCenter.longitude, zoom: 15.0)
                radiusMapView = GMSMapView.map(withFrame: CGRect.init(x: 0, y: 0, width: mapView.frame.size.width, height: mapView.frame.size.height), camera: camera)
                radiusMapView.mapType = .normal
                radiusMapView.delegate = self
                radiusMapView.isMyLocationEnabled = true
                self.mapView.addSubview(radiusMapView)
                self.perform(#selector(self.drowCircle), with: nil, afterDelay: 1.0)

            }

        }
        //   currentLocation?.coordinate.latitude = "23.1008576"
        // currentLocation?.coordinate.longitude = "72.5260449"
        
    }
    
    @objc func drowCircle() {
        // Creates a marker in the center of the map.
        
        let marker = GMSMarker()
        marker.isDraggable = true
        //        marker.icon = UIImage(named: "markerPin.png")!
        marker.position = circleCenter
        marker.map = radiusMapView
        radiusCircle = GMSCircle(position: circleCenter, radius: 100.0)
        radiusValue = 100
        radiusCircle.fillColor = UIColor(red: CGFloat(1.0), green: CGFloat(0), blue: CGFloat(0), alpha: CGFloat(0.10))
        radiusCircle.strokeColor = UIColor.red
        radiusCircle.strokeWidth = 0.5
        radiusCircle.map = radiusMapView
        APPDELEGATE.removeChargementLoader()
    }
    
    func setRadius(radius: CGFloat) {
        radiusCircle.radius = CLLocationDistance(radius)
    }
    @IBAction func btnToPressed(_ sender: UIButton) {
//        strCheckFrom = "to"
//
//        //  setButtonForView(sender: sender)
//        if(viewAddress.isHidden == true){
//            self.setView(view: viewAddress, hidden: false)
//        }
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ListOfAddressVC") as! ListOfAddressVC
        nextViewController.checkBoolFrom = false
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }

    @IBAction func btnFromPressed(_ sender: UIButton) {
//        strCheckFrom = "from"
//        //setButtonForView(sender: sender)
//        if(viewAddress.isHidden == true){
//            self.setView(view: viewAddress, hidden: false)
//        }

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ListOfAddressVC") as! ListOfAddressVC
        nextViewController.checkBoolFrom = true
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    func setView(view: UIView, hidden: Bool) {
        if(hidden){
            UIView.transition(with: view, duration: 1, options: .transitionFlipFromRight, animations: {
                self.btnFrom.alpha = 1
                self.btnTo.alpha = 1
                view.isHidden = true
            })

        }else{
            UIView.transition(with: view, duration: 1, options: .transitionFlipFromLeft, animations: {
                self.btnFrom.alpha = 0.4
                self.btnTo.alpha = 0.4
                view.isHidden = false
            })

        }
//        self.setView(view: viewAddress, hidden: false)

    }

    
    func setButtonForView(sender : UIButton){
        sender.layer.borderWidth = 1.0
        if(sender == btnSelectAddessList || sender == btnSaveAddress){
            sender.layer.borderColor = UIColor.white.cgColor

        }else{
        }
        if(sender == btnSelectAddessList || sender == btnSaveAddress){
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.setTitleColor(UIColor.white, for: .selected)

        }else{

        }
        sender.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)
        sender.layer.cornerRadius = 15.0

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAddress.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblViewlistAddress.dequeueReusableCell(withIdentifier: "cell") as! custCellLocation
        let data = arrayAddress[indexPath.row]
        let stringName = data["name"] as! String
        cell.lblCityName.text = "\(stringName)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataDict = arrayAddress[indexPath.row]

        txtname.text = dataDict["name"] as? String
        txtFullAddress.text = dataDict["addressString"] as? String
        txtmobile.text = dataDict["Mobile"] as? String
        txtname.text = dataDict["name"] as? String
        txtmobile.text = dataDict["Mobile"] as? String
        txtStreetNo.text = dataDict["streetNo"] as? String
        txtStreetName.text = dataDict["Street"] as? String
        txtPostalCode.text = dataDict["pincode"] as? String
        txtCity.text = dataDict["city"] as? String
        txtState.text = dataDict["state"] as? String
        txtCountry.text = dataDict["country"] as? String
       
        if(strCheckFrom == "from"){
            Fromlatitude = dataDict["latitude"] as? String
            Fromlongitude = dataDict["longitude"] as? String

        }else{
            Tolongitude = dataDict["longitude"] as? String
            Tolatitude = dataDict["latitude"] as? String

        }

        tblViewlistAddress.isHidden = true

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
     //   tblVIew.isHidden = true

        return true
    }
   

    func didSelectAutoSuggestResult(place: GMSPlace) {
        txtFullAddress.text = place.formattedAddress
        if(strCheckFrom == "from"){
            Fromlatitude = "\(place.coordinate.latitude)"
            Fromlongitude = "\(place.coordinate.longitude)"

        }
        else{
            Tolatitude = "\(place.coordinate.latitude)"
            Tolongitude = "\(place.coordinate.longitude)"

        }
        
        
        if let addressComp = place.addressComponents
        {
            for obj in addressComp
            {
                if obj.type == "street_number"
                {
                    txtStreetNo.text = obj.name
                }
                
                if obj.type == "route"
                {
                    txtStreetName.text = obj.name
                }
                
                if obj.type == "country"
                {
                    txtCountry.text = obj.name
                }
                
                if obj.type == "postal_code"
                {
                    txtPostalCode.text = obj.name
                }
                
                if obj.type == "administrative_area_level_2"
                {
                    txtCity.text = obj.name
                }
                
                if obj.type == "administrative_area_level_1"
                {
                    txtState.text = obj.name
                }
            }
            
        }
        
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
