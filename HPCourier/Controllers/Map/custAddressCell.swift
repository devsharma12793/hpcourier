//
//  custAddressCell.swift
//  HPCourier
//
//  Created by Devendra on 14/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class custAddressCell: UITableViewCell {

    @IBOutlet weak var viewOrder: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet var btnDeleteAddress: UIButton!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
