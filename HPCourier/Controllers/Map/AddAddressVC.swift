//
//  AddAddressVC.swift
//  HPCourier
//
//  Created by Devendra on 15/08/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

class AddAddressVC: UIViewController,UITextFieldDelegate,GMSAutocompleteCustomDelegate,GMSMapViewDelegate,CLLocationManagerDelegate {
  
    @IBOutlet weak var txtFullAddress: GMSAutoCompleteTextfield!
    var sliderValue:Int = 100
    var radiusMapView: GMSMapView!
    var radiusCircle: GMSCircle!
    var radiusValue = 0
    var actualLocation: CLLocation!
    var locationLatitude = CLLocationDegrees()
    var locationLongitude = CLLocationDegrees()
    var addressString = ""
    var circleCenter = CLLocationCoordinate2D()
    var currentLocation: CLLocation?
    var contact = contacts
    @IBOutlet weak var mapView: UIView!
    var strCheckFrom:String = ""
    @IBOutlet weak var txtname: SLTextField!
    @IBOutlet weak var txtmobile: SLTextField!
    @IBOutlet weak var txtStreetNo: SLTextField!
    @IBOutlet weak var txtStreetName: SLTextField!
    @IBOutlet weak var txtPostalCode: SLTextField!
    @IBOutlet weak var txtCity: SLTextField!
    @IBOutlet weak var txtState: SLTextField!
    @IBOutlet weak var txtCountry: SLTextField!
    var Fromlatitude : String?
    var Fromlongitude : String?
    
    var Tolatitude : String?
    var Tolongitude : String?
    var callMapView : Bool = true
   
    var contactData = AddressDict()
    var arrayAddress = [JSONDictionary]()
    var boolcheckaddin : Bool = false
    var to_user_nameS : String = ""
    var to_user_phoneS : String = ""
    var from_user_nameS : String = ""
    var from_user_phoneS : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        txtFullAddress.autoCompleteDelegate = self

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSaveAddressPressed(_ sender: UIButton) {
        
        if self.validateAllData() == true {
            //AddressDict.toJsonDict(<#T##AddressDict#>)
            let name = txtname.text
            let mobile = txtmobile.text
            let strAddress = txtFullAddress.text
            self.addAddressed(name: name!, address: strAddress!, number: mobile!, latitude: Fromlatitude!, longitude: Fromlongitude!)
        }
    }

    
    func addAddressed(name: String, address : String, number:String, latitude:String, longitude:String){
        
        let data = AddressNew()
        
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                data.user_id = user["id"] as? String
                
            }
        }
        
        data.name = name
        data.address = address
        data.number = number
        data.latitude = latitude
        data.longitude = longitude
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.Addaddress(addressData:data,completion: { (error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
//
                
                
                    SLAlert.showAlertOK(message: "Address Added Successfully...!", completion: {
                        self.navigationController?.popViewController(animated: true)

                    })
            
            }
        })

//        user_id = 51
//        name = Chintan
//        address = Surat gujarat india
//        number = 1234567890
//        latitude = 123456
//        longitude = 123456
        
        
        
    }
    func validateAllData() -> Bool {
        let straddress = txtFullAddress.text
        let strstreetNo = txtStreetNo.text
        let strstreetName = txtStreetName.text
        let strpostalcode = txtPostalCode.text
        let strcity = txtCity.text
        let strstate = txtState.text
        let strcountry = txtCountry.text
        
        if straddress?.count == 0 || straddress == "" {
            SLAlert.showAlert(str: "PLease Enter Address")
            return false
        }
        else if strstreetNo?.count == 0 || strstreetNo == "" {
            SLAlert.showAlert(str: "PLease Enter Street/Flat No")
            return false
        }
        else if strstreetName?.count == 0 || strstreetName == "" {
            SLAlert.showAlert(str: "PLease Enter StreetName")
            return false
        }
        else if strpostalcode?.count == 0 || strpostalcode == "" {
            SLAlert.showAlert(str: "PLease Enter PostalCode")
            return false
        }
        else if strcity?.count == 0 || strcity == "" {
            SLAlert.showAlert(str: "PLease Enter City Name")
            return false
        }
        else if strstate?.count == 0 || strstate == "" {
            SLAlert.showAlert(str: "PLease Enter State Name")
            return false
        }
        else if strcountry?.count == 0 || strcountry == "" {
            SLAlert.showAlert(str: "PLease Enter Country Name")
            return false
        }
        else {
            
                if Fromlatitude?.count == 0 || Fromlatitude == "" || Fromlatitude == nil {
                    SLAlert.showAlert(str: "Location Not Found")
                    return false
                }
                else if Fromlongitude?.count == 0 || Fromlongitude == "" || Fromlongitude == nil {
                    SLAlert.showAlert(str: "Location Not Found")
                    return false
                }
            txtFullAddress.resignFirstResponder()
            txtStreetNo.resignFirstResponder()
            txtStreetName.resignFirstResponder()
            txtPostalCode.resignFirstResponder()
            txtCity.resignFirstResponder()
            txtState.resignFirstResponder()
            txtCountry.resignFirstResponder()
            //self.getLoginWebService()
        }
        
        return true
    }
  

    func didSelectAutoSuggestResult(place: GMSPlace) {
     
        txtFullAddress.text = place.formattedAddress
            Fromlatitude = "\(place.coordinate.latitude)"
            Fromlongitude = "\(place.coordinate.longitude)"
      
        if let addressComp = place.addressComponents
        {
            for obj in addressComp
            {
                if obj.type == "street_number"
                {
                    txtStreetNo.text = obj.name
                }
                
                if obj.type == "route"
                {
                    txtStreetName.text = obj.name
                }
                
                if obj.type == "country"
                {
                    txtCountry.text = obj.name
                }
                
                if obj.type == "postal_code"
                {
                    txtPostalCode.text = obj.name
                }
                
                if obj.type == "administrative_area_level_2"
                {
                    txtCity.text = obj.name
                }
                
                if obj.type == "administrative_area_level_1"
                {
                    txtState.text = obj.name
                }
            }
            
        }
        
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
