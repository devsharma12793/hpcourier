//
//  OrderDetailVC.swift
//  HPCourier
//
//  Created by Devendra on 08/06/18.
//  Copyright © 2018 Devendra Sharma. All rights reserved.
//

import UIKit

class OrderDetailVC: UIViewController,UITextViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var txtOthersKG: UITextField!
    @IBOutlet weak var txtGoodKg: UITextField!
    @IBOutlet weak var btnCheckboxSimpleCourier: UIButton!
    @IBOutlet weak var btnCheckBoxGoods: UIButton!
    @IBOutlet weak var btnCheckBoxOthers: UIButton!
    var branchDetail : AnyObject?
    var UserDetailForOrder : JSONDictionary = [:]
    var courierBoyDetail : JSONDictionary = [:]
    
    @IBOutlet weak var txtGoodsheightconst: NSLayoutConstraint!
    @IBOutlet weak var txtOtherHeightCOnst: NSLayoutConstraint!
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var txtViewForOther: UITextView!
    @IBAction func btnBackPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    var strTotalKg : String = ""
    
    @IBOutlet var btnTerms: UIButton!
    @IBAction func btnTermsPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func btnTermsandConditionPressed(_ sender: UIButton) {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WebVC") as! WebVC
            nextViewController.strWebURL = "http://hpcourier.com/term_condition.html"
            nextViewController.strViewTitle = "TERMS"

            self.navigationController?.pushViewController(nextViewController, animated: true)
            

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        txtGoodKg.layer.borderColor = colorOrange.cgColor
        txtOthersKG.layer.borderColor = colorOrange.cgColor
        txtGoodKg.layer.borderWidth = 1
        txtOthersKG.layer.borderWidth = 1

        txtViewForOther.layer.cornerRadius = 10
        txtViewForOther.layer.borderWidth = 1
        txtViewForOther.layer.borderColor = colorOrange.cgColor
        self.setImage(sender: btnCheckBoxGoods, selectedimage:#imageLiteral(resourceName: "checked"), unselectedImg: #imageLiteral(resourceName: "unchecked"))
        self.setImage(sender: btnCheckboxSimpleCourier, selectedimage:#imageLiteral(resourceName: "checked"), unselectedImg: #imageLiteral(resourceName: "unchecked"))
        self.setImage(sender: btnCheckBoxOthers, selectedimage:#imageLiteral(resourceName: "checked"), unselectedImg: #imageLiteral(resourceName: "unchecked"))
        self.setImage(sender: btnTerms, selectedimage:#imageLiteral(resourceName: "checked"), unselectedImg: #imageLiteral(resourceName: "unchecked"))

        txtGoodsheightconst.constant = 0
        txtOtherHeightCOnst.constant = 0
        txtViewForOther.text = "Specify Courier IN Detail"
        txtViewForOther.textColor = UIColor.lightGray
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setImage(sender:UIButton,selectedimage:UIImage,unselectedImg : UIImage){
        sender.setImage(unselectedImg, for: .normal)
        sender.setImage(selectedimage, for: .selected)
        
    }
    @IBAction func btnCheckAmountPressed(_ sender: UIButton) {
        
        //        if(btnCheckboxSimpleCourier.isSelected == false && btnCheckBoxGoods.isSelected == false && btnCheckBoxOthers.isSelected == false){
        //            SLAlert.showAlert(str: "Please Select Type Of Courier")
        //        }else if(btnCheckBoxOthers.isSelected ==  true){
        //            let strOther = txtViewForOther.text
        //            if(strOther != nil || strOther != "" || strOther?.characters.count != 0){
        //
        //                self.getTotalAnount()
        //
        //            }else{
        //                SLAlert.showAlert(str: "Please Enter Description of Other Amount")
        //
        //            }
        //        }else{
        //            self.getTotalAnount()
        //        }
        //
    }
    var courierType : String = ""
    
    @IBAction func btnSubmitPressed(_ sender: UIButton) {
        if(btnTerms.isSelected == false)
        {
            SLAlert.showAlert(str: "Please Agree With Terms & Connditions")
        }else if(txtGoodKg.text == "" && txtOthersKG.text == "" && btnCheckboxSimpleCourier.isSelected == false){
            SLAlert.showAlert(str: "Please select courier type or enter weight of selected courier")

        }else if(lblTotalAmount.text == ""){
            SLAlert.showAlert(str: "Amount Not Getting")
            
        }else{
            self.submitOrder()

        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        strTotalKg = ""
        if(textField.text != "")
        {
            if(textField == txtGoodKg)
            {
                strTotalKg = txtGoodKg.text!
                self.getTotalAnount()
                
            }else if(textField == txtOthersKG){
                strTotalKg = txtOthersKG.text!
                self.getTotalAnount()
                
            }else{
                strTotalKg = ""
            }
            
            
        }else{
            
        }

    }

    func submitOrder(){
        
        let dict = self.toJsonDict()
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CourierBoyListVC") as! CourierBoyListVC
       nextViewController.orderDetail = dict
       self.navigationController?.pushViewController(nextViewController, animated: true)
//        APPDELEGATE.addChargementLoader()
//        APIManager.sharedInstance.AddOrder(dictData: dict, completion: { (json:AnyObject?,error:NSError?) in
//            APPDELEGATE.removeChargementLoader()
//            if error == nil {
//                if(json != nil){
//
//                    let alert = UIAlertController(title: "Hp Courier", message: "Courier Placed Successfully", preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HomeMapVC") as! HomeMapVC
//                        self.navigationController?.pushViewController(nextViewController, animated: true)
//
//                    }))
//
//                    self.present(alert, animated: true)
//
//
//                }else{
//
//                }
//            }
//        })
//
        
    }
    func toJsonDict() -> JSONDictionary {
        var dict: JSONDictionary = [:]
        
        let fromaddress = UserDetailForOrder[kfrom_address] as! String
        let toaddress = UserDetailForOrder[kto_address] as! String
        let toUserName = UserDetailForOrder["to_user_name"] as! String
        let to_user_phone = UserDetailForOrder["to_user_phone"] as! String
        let fromUserName = UserDetailForOrder["from_user_name"] as! String
        let from_user_phone = UserDetailForOrder["from_user_phone"] as! String

        let branchData = branchDetail?.object(at: 0) as? JSONDictionary
        var to_branch_id : String = ""
        var from_branch_id : String = ""

        if let to_branchId = branchData!["to_branch_id"]{
            to_branch_id = to_branchId as! String
        }
        if let to_branchId = branchData!["to_branch"]{
            to_branch_id = to_branchId as! String
        }
        if let from_branchId = branchData!["from_branch_id"]{
            from_branch_id = from_branchId as! String
        }
        if let from_branchId = branchData!["from_branch"]{
            from_branch_id = from_branchId as! String
        }


        let kgs = strTotalKg
        let amount = lblTotalAmount.text
        let courier_type = courierType
//        let courier_boy_id = courierBoyDetail["id"]
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                let user_id = user["id"]
                dict["user_id"]  = user_id! as AnyObject?
                
            }
        }
        
        let from_latitude = UserDetailForOrder[kfrom_latitude] as! String
        let from_longitude = UserDetailForOrder[kfrom_longitude] as! String
        let to_latitude = UserDetailForOrder[kto_latitude] as! String
        let to_longitude = UserDetailForOrder[kto_longitude] as! String
        
        dict[kfrom_address]  = fromaddress as AnyObject?
        dict[kto_address]  = toaddress as AnyObject?
        dict["to_user_name"]  = toUserName as AnyObject?
        dict["to_user_phone"]  = to_user_phone as AnyObject?
        dict["from_user_name"]  = fromUserName as AnyObject?
        dict["from_user_phone"]  = from_user_phone as AnyObject?
        dict["to_branch_id"]  = to_branch_id as AnyObject?
        dict["from_branch_id"]  = from_branch_id as AnyObject?

        dict["kgs"]  = kgs as AnyObject?
        dict["amount"]  = amount! as AnyObject?
        dict["courier_type"]  = courier_type as AnyObject?
//        dict["courier_boy_id"]  = courier_boy_id as AnyObject?
        dict["from_latitude"]  = from_latitude as AnyObject?
        dict["from_longitude"]  = from_longitude as AnyObject?
        dict["to_latitude"]  = to_latitude as AnyObject?
        dict["to_longitude"]  = to_longitude as AnyObject?
        dict["desc"] = "this is Test Data" as AnyObject
        return dict
    }
    
    
    @IBAction func btnCheckBOxPressed(_ sender: UIButton) {
        lblTotalAmount.text = ""
        btnCheckboxSimpleCourier.isSelected = false
        btnCheckBoxGoods.isSelected = false
        btnCheckBoxOthers.isSelected = false
        sender.isSelected = !sender.isSelected
        
        if(sender == btnCheckboxSimpleCourier){
            courierType = "1"
            self.getTotalAnount()
            
        }
        if(sender == btnCheckBoxGoods){
            courierType = "2"
            txtGoodsheightconst.constant = 30
        }else{
            txtGoodsheightconst.constant = 0
            
        }
        if(sender == btnCheckBoxOthers){
            courierType = "4"
            txtOtherHeightCOnst.constant = 30
            //txtViewForOther.isHidden = false
        }else{
            txtOtherHeightCOnst.constant = 0
            // txtViewForOther.isHidden = true
            
        }
        //        if(btnCheckboxSimpleCourier.isSelected == false && btnCheckBoxGoods.isSelected == false && btnCheckBoxOthers.isSelected == false){
        //            SLAlert.showAlert(str: "Please Select Type Of Courier")
        //        }else if(btnCheckBoxOthers.isSelected ==  true){
        //            let strOther = txtViewForOther.text
        //            if(strOther != nil || strOther != "" || strOther?.characters.count != 0){
        //
        //                self.getTotalAnount()
        //
        //            }else{
        //                SLAlert.showAlert(str: "Please Enter Description of Other Amount")
        //
        //            }
        //        }else{
        //            self.getTotalAnount()
        //        }
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtViewForOther.textColor == UIColor.lightGray {
            txtViewForOther.text = nil
            txtViewForOther.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if txtViewForOther.text.isEmpty {
            txtViewForOther.text = "Placeholder"
            txtViewForOther.textColor = UIColor.lightGray
        }
        txtViewForOther.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        strTotalKg = ""
        if(textField.text != "")
        {
            if(textField == txtGoodKg)
            {
                strTotalKg = txtGoodKg.text!
                self.getTotalAnount()
                
            }else if(textField == txtOthersKG){
                strTotalKg = txtOthersKG.text!
                self.getTotalAnount()
                
            }else{
                strTotalKg = ""
            }
            
            
        }else{
            
        }
        
        //        if(btnCheckboxSimpleCourier.isSelected == false && btnCheckBoxGoods.isSelected == false && btnCheckBoxOthers.isSelected == false){
        //            SLAlert.showAlert(str: "Please Select Type Of Courier")
        //        }else if(btnCheckBoxOthers.isSelected ==  true){
        //
        //            let strOther = txtViewForOther.text
        //
        //            if(strOther != nil || strOther != "" || strOther?.characters.count != 0){
        //
        //                self.getTotalAnount()
        //
        //            }else{
        //                //SLAlert.showAlert(str: "Please Enter Description of Other Amount")
        //
        //            }
        //        }else{
        //            self.getTotalAnount()
        //        }
        
        self.view.endEditing(true)
        return true
    }
    
    func getTotalAnount(){
        
        let kgs = strTotalKg
        let str = courierType
        var userID : String = ""
        if let userData = UserDefaults.standard.object(forKey: kAPPUser) as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? JSONDictionary {
                // self.populateWithJson(dict: user)
                let user_id = user["id"]
                userID  = user_id! as! String
                
            }
        }
        let branchData = branchDetail?.object(at: 0) as? JSONDictionary
        var frombranchID : String = ""
        var tobranchID : String = ""

        if let fromBranchID = branchData!["from_branch_id"] as? String{
            frombranchID = fromBranchID
        }
        if let fromBranchID = branchData!["from_branch"] as? String{
            frombranchID = fromBranchID
        }

        if let toBranchID = branchData!["to_branch_id"] as? String{
            tobranchID = toBranchID
        }
        if let toBranchID = branchData!["to_branch"] as? String{
            tobranchID = toBranchID
        }

        
        APPDELEGATE.addChargementLoader()
        
        APIManager.sharedInstance.GetTotalAmt(strFromBranchID: frombranchID, strToBranchID: tobranchID, strcourierType: str, strkg: kgs, strUserID: userID, completion: {(json:AnyObject?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if(json != nil){
                    let data : JSONDictionary = (json?.object(at: 0) as? JSONDictionary)!//[0] as? JSONDictionary as! JSONDictionary
                    let amount = data["amount"] as! Double
                    self.lblTotalAmount.text = "\(String(describing: amount))"
                }else{
                    SLAlert.showAlert(str: "Something Wrong")
                }
            }
        })
        
        
        
        //        APIManager.sharedInstance.GetTotalAmt(strBranchID:"111",strcourierType : str, strkg : kgs!, completion: { (json:AnyObject?,error:NSError?) in
        //            APPDELEGATE.removeChargementLoader()
        //            if error == nil {
        //                if(json != nil){
        //                    let data : JSONDictionary = (json?.object(at: 0) as? JSONDictionary)!//[0] as? JSONDictionary as! JSONDictionary
        //                    let amount = data["amount"] as! Int
        //                    self.lblTotalAmount.text = "\(String(describing: amount))"
        //                }else{
        //                    SLAlert.showAlert(str: "Something Wrong")
        //                }
        //            }
        //        })
        
    }
    //    1 = Simple
    //    2 = Goods
    //    3 = Money (Removed)
    //    4 = Other
    
    
    //    func toJsonDict() -> JSONDictionary {
    //        var dict: JSONDictionary = [:]
    //        let branch_id = "112"
    //        let courier_type = "1"
    //        dict["branch_id"] = branch_id as AnyObject?
    //        dict["courier_type"] = courier_type as AnyObject?
    //        dict["kgs"] = kgs as AnyObject?
    
    //     return dict
    //    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
